package org.nrg.xams.xchange.services.storage;

import com.thedeanda.lorem.Lorem;
import com.thedeanda.lorem.LoremIpsum;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.nrg.test.workers.resources.ResourceManager;
import org.nrg.xams.xchange.services.storage.domain.auditing.EntityHistory;
import org.nrg.xams.xchange.services.storage.domain.entities.ArchiveEntry;
import org.nrg.xams.xchange.services.storage.domain.entities.ArchiveFolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.entry;
import static org.nrg.xams.xchange.services.storage.PathAndFileUtils.*;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("postgresql")
@ContextConfiguration(classes = StorageServiceTestsConfig.class)
@ComponentScan("org.nrg.xams.xchange.services.storage.domain")
@Slf4j
class XChangeStorageServiceTests {
    @AfterEach
    void cleanUp() {
        final StopWatch     timer   = StopWatch.createStarted();
        final AtomicInteger counter = new AtomicInteger(0);
        Stream.of(Paths.get(_properties.getStorageLocation()), (_workingDirectory))
              .filter(Objects::nonNull)
              .map(Path::toFile)
              .filter(File::exists)
              .filter(File::isDirectory)
              .forEach(folder -> {
                  try {
                      FileUtils.deleteDirectory(folder);
                      counter.getAndIncrement();
                  } catch (IOException e) {
                      log.error("An error occurred trying to delete the folder '{}'", folder, e);
                  }
              });
        timer.stop();
        log.info("Deleted {} folders in {} ms.", counter.get(), timer.getTime());
    }

    @Test
    void contextLoads() {
        log.info("The context loads, so that's good.");
    }

    @Test
    void testPathMaps() throws IOException {
        final Map<Path, Pair<Resource, String>> fileSet1 = buildFileSet(getWorkingFolder("fileSet1"),
                                                                        RESOURCES.stream(),
                                                                        path -> RESOURCE_MANAGER.getTestResourceFile(Paths.get("fileSet1", path.toString()).toString()));
        final Map<Path, Pair<Resource, String>> fileSet2 = buildFileSet(getWorkingFolder("fileSet2"),
                                                                        RESOURCES.stream(),
                                                                        path -> RESOURCE_MANAGER.getTestResourceFile(Paths.get("fileSet2", path.toString()).toString()));

        assertThat(fileSet1).isNotNull().isNotEmpty().hasSize(fileSet2.size());
        assertThat(fileSet1).containsKeys(fileSet2.keySet().toArray(new Path[0]));
    }

    @Test
    void testBasicStorageService() throws IOException {
        final Map<Path, Pair<Resource, String>> fileSet1 = buildFileSet(getWorkingFolder("fileSet1"),
                                                                        RESOURCES.stream(),
                                                                        path -> RESOURCE_MANAGER.getTestResourceFile(Paths.get("fileSet1", path.toString()).toString()));
        final Map<Path, Resource> paths1   = fileSet1.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().getLeft()));
        final Map<Path, String>   digests1 = fileSet1.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().getRight()));

        final ArchiveFolder initial = _service.store("Basic Archive Folder", Paths.get("basicArchiveFolder"), paths1);
        assertThat(initial).isNotNull()
                           .hasFieldOrPropertyWithValue("name", "Basic Archive Folder")
                           .hasFieldOrPropertyWithValue("path", "basicArchiveFolder");

        final Set<ArchiveEntry> entries1 = initial.getEntries();
        assertThat(entries1).isNotNull()
                            .isNotEmpty()
                            .hasSize(3);
        entries1.forEach(entry -> assertThat(entry).hasFieldOrPropertyWithValue("digest", digests1.get(entry.getPathAsPath())));

        final Map<Path, Pair<Resource, String>> fileSet2 = buildFileSet(getWorkingFolder("fileSet2"),
                                                                        RESOURCES.stream(),
                                                                        path -> RESOURCE_MANAGER.getTestResourceFile(Paths.get("fileSet2", path.toString()).toString()));
        final Map<Path, Resource> paths2   = fileSet2.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().getLeft()));
        final Map<Path, String>   digests2 = fileSet2.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().getRight()));

        final ArchiveFolder updated = _service.store("Basic Archive Folder", Paths.get("basicArchiveFolder"), paths2);
        assertThat(updated).isNotNull()
                           .hasFieldOrPropertyWithValue("name", "Basic Archive Folder")
                           .hasFieldOrPropertyWithValue("path", "basicArchiveFolder")
                           .hasFieldOrPropertyWithValue("id", initial.getId());
        final Set<ArchiveEntry> entries2 = updated.getEntries();
        assertThat(entries2).isNotNull()
                            .isNotEmpty()
                            .hasSize(3);
        entries2.forEach(entry -> assertThat(entry).hasFieldOrPropertyWithValue("digest", digests2.get(entry.getPathAsPath())));

        final Map<Path, Resource> resources = _service.getResources(initial);
        assertThat(resources).isNotNull().isNotEmpty().hasSize(3);
        assertThat(resources.keySet()).containsExactlyInAnyOrderElementsOf(RESOURCES);
    }

    @Test
    void testCommonPathElements() {
        final Path       root      = Paths.get("foo");
        final Path       path1     = Paths.get("foo/bar1/baz1");
        final Path       path2     = Paths.get("foo/bar1/baz2");
        final Path       path3     = Paths.get("foo/bar2/baz1");
        final Path       path4     = Paths.get("foo/bar2/baz2");
        final List<Path> pathList  = Arrays.asList(path1, path2, path3, path4);
        final Path[]     pathArray = pathList.toArray(new Path[0]);

        final Path simpleAncestor = getCommonAncestor(path1, path2);
        final Path rootFromList   = getCommonAncestor(pathList);
        final Path rootFromArray  = getCommonAncestor(pathArray);

        assertThat(simpleAncestor).isNotNull().isEqualTo(Paths.get("foo/bar1"));
        assertThat(rootFromList).isNotNull().isEqualTo(root);
        assertThat(rootFromArray).isNotNull().isEqualTo(root);
    }

    @Test
    void testComplexStorageService() throws IOException {
        final Path                              fileSetPath = getWorkingFolder("fileSet");
        final FileGenerator                     mapper      = new FileGenerator(fileSetPath);
        final Map<Path, Pair<Resource, String>> fileSet     = buildFileSet(fileSetPath, Files.lines(FILE_LIST).map(Paths::get), mapper);
        final int                               fileSetSize = fileSet.size();

        final Map<Path, Resource> paths   = fileSet.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().getLeft()));
        final Map<Path, String>   digests = fileSet.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().getRight()));

        final Path          archiveFolderPath = getWorkingFolder("archiveFolder");
        final ArchiveFolder archiveFolder     = _service.store("Archive Folder", archiveFolderPath, paths);
        assertThat(archiveFolder).isNotNull()
                                 .hasFieldOrPropertyWithValue("name", "Archive Folder")
                                 .hasFieldOrPropertyWithValue("path", archiveFolderPath.toString());

        final Set<ArchiveEntry> entries = archiveFolder.getEntries();
        assertThat(entries).isNotNull()
                           .isNotEmpty()
                           .hasSize(fileSetSize);
        entries.forEach(entry -> assertThat(entry).hasFieldOrPropertyWithValue("digest", digests.get(entry.getPathAsPath())));

        final Path                              changeSet1Folder    = getWorkingFolder("changeSet1");
        final FileGenerator                     changeSet1Mapper    = new FileGenerator(changeSet1Folder);
        final Map<Path, Pair<Resource, String>> modifiedChangeSet1  = buildModifiedChangeSet(changeSet1Folder, fileSet, changeSet1Mapper);
        final Map<Path, Pair<Resource, String>> addedChangeSet1     = buildAddedChangeSet(changeSet1Folder, fileSet, changeSet1Mapper);
        final int                               addedChangeSet1Size = addedChangeSet1.size();
        final Map<Path, Pair<Resource, String>> changeSet1          = combineChangeSets(modifiedChangeSet1, addedChangeSet1);

        final Map<Path, Resource> changeSet1Paths         = changeSet1.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().getLeft()));
        final Map<Path, String>   changeSet1Digests       = changeSet1.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().getRight()));
        final ArchiveFolder       changeSet1ArchiveFolder = _service.store(archiveFolderPath, changeSet1Paths);
        final Set<ArchiveEntry>   changeSet1Entries       = changeSet1ArchiveFolder.getEntries();

        assertThat(changeSet1ArchiveFolder).isNotNull()
                                           .hasFieldOrPropertyWithValue("name", "Archive Folder")
                                           .hasFieldOrPropertyWithValue("path", archiveFolderPath.toString());
        assertThat(changeSet1Entries).isNotNull()
                                     .isNotEmpty()
                                     .hasSize(fileSetSize + addedChangeSet1Size);
        changeSet1Entries.forEach(entry -> assertThat(entry).hasFieldOrPropertyWithValue("digest", changeSet1Digests.containsKey(entry.getPathAsPath()) ? changeSet1Digests.get(entry.getPathAsPath()) : digests.get(entry.getPathAsPath())));

        final List<Path>        deletionPaths         = buildDeletionChangeSet(new ArrayList<>(entries));
        final ArchiveFolder     deletionArchiveFolder = _service.delete(changeSet1ArchiveFolder, deletionPaths);
        final Set<ArchiveEntry> deletionEntries       = deletionArchiveFolder.getEntries();

        assertThat(deletionArchiveFolder).isNotNull()
                                         .hasFieldOrPropertyWithValue("name", "Archive Folder")
                                         .hasFieldOrPropertyWithValue("path", archiveFolderPath.toString());
        assertThat(deletionEntries).isNotNull()
                                   .isNotEmpty()
                                   .hasSize(fileSetSize + addedChangeSet1Size - deletionPaths.size());
        deletionEntries.forEach(entry -> assertThat(entry).hasFieldOrPropertyWithValue("digest", changeSet1Digests.containsKey(entry.getPathAsPath()) ? changeSet1Digests.get(entry.getPathAsPath()) : digests.get(entry.getPathAsPath())));

        final List<EntityHistory<ArchiveFolder>> archiveFoldersHistory   = _service.getHistory(deletionArchiveFolder);
        final List<Number>                       archiveFoldersRevisions = _service.getRevisions(deletionArchiveFolder);

        assertThat(archiveFoldersHistory).isNotNull().isNotEmpty().hasSize(3);
        assertThat(archiveFoldersRevisions).isNotNull().isNotEmpty().hasSize(3);

        final Map<Path, Resource> resources = _service.getResources(deletionArchiveFolder);
        assertThat(resources).isNotNull().isNotEmpty().hasSize(deletionEntries.size());
    }

    private Map<Path, Pair<Resource, String>> buildFileSet(final Path directory, final Stream<Path> paths, final Function<Path, File> mapper) {
        final StopWatch timer = StopWatch.createStarted();

        try {
            log.info("Creating file set in the folder {}", directory);
            return paths.collect(Collectors.toMap(Function.identity(), path -> generateFile(directory, path, mapper)));
        } finally {
            timer.stop();
            log.info("Created requested files in the folder {} in {} ms.", directory, timer.getTime());
        }
    }

    private Map<Path, Pair<Resource, String>> buildModifiedChangeSet(final Path directory, final Map<Path, Pair<Resource, String>> fileSet, final Function<Path, File> mapper) {
        final StopWatch timer = StopWatch.createStarted();

        final List<Path> paths         = new ArrayList<>(fileSet.keySet());
        final int        size          = paths.size();
        final int        modifiedCount = RandomUtils.nextInt(size / 20, size / 5);

        try {
            log.info("Generating {} modified files on a file set of {} entries", modifiedCount, paths.size());
            return new Random().ints(modifiedCount, 0, size).distinct()
                               .mapToObj(paths::get)
                               .collect(Collectors.toMap(Function.identity(), path -> generateFile(directory, path, mapper)));
        } finally {
            timer.stop();
            log.info("Created {} modified files in the folder {} in {} ms.", modifiedCount, directory, timer.getTime());
        }
    }

    private Map<Path, Pair<Resource, String>> buildAddedChangeSet(final Path directory, final Map<Path, Pair<Resource, String>> fileSet, final Function<Path, File> mapper) {
        final StopWatch timer = StopWatch.createStarted();

        final List<Path> paths    = new ArrayList<>(fileSet.keySet());
        final int        size     = paths.size();
        final List<Path> folders  = fileSet.keySet().stream().map(Path::getParent).distinct().collect(Collectors.toList());
        final int        addCount = RandomUtils.nextInt(0, size / 100);

        try {
            log.info("Generating {} added files on a file set of {} entries", addCount, paths.size());
            return IntStream.range(0, addCount)
                            .mapToObj(index -> generateRandomFile(folders))
                            .collect(Collectors.toMap(Function.identity(), path -> generateFile(directory, path, mapper)));
        } finally {
            timer.stop();
            log.info("Created {} added files in the folder {} in {} ms.", addCount, directory, timer.getTime());
        }
    }

    private List<Path> buildDeletionChangeSet(final List<ArchiveEntry> entries) {
        final int size          = entries.size();
        final int deletionCount = RandomUtils.nextInt(0, size / 100);
        return new Random().ints(deletionCount, 0, size).distinct()
                           .mapToObj(entries::get)
                           .map(ArchiveEntry::getPathAsPath)
                           .collect(Collectors.toList());
    }

    private Map<Path, Pair<Resource, String>> combineChangeSets(final Map<Path, Pair<Resource, String>> first, final Map<Path, Pair<Resource, String>> second) {
        return Stream.concat(first.entrySet().stream(), second.entrySet().stream()).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    private Path generateRandomFile(final List<Path> folders) {
        final int size  = folders.size();
        final int index = RandomUtils.nextInt(0, size + (size / 10));
        return (index < folders.size() ? folders.get(index) : generateRandomFolder()).resolve(generateRandomFilename());
    }

    private String generateRandomFilename() {
        final int index = RandomUtils.nextInt(0, EXTENSIONS.size() + 8);
        return RandomStringUtils.randomAlphabetic(10, 30) + "." + (index < EXTENSIONS.size() ? EXTENSIONS.get(index) : RandomStringUtils.randomAlphabetic(1, 6));
    }

    private Path generateRandomFolder() {
        return Paths.get(RandomStringUtils.randomAlphabetic(5, 20), IntStream.range(0, RandomUtils.nextInt(1, 8)).mapToObj(index -> RandomStringUtils.randomAlphanumeric(5, 20)).distinct().toArray(String[]::new));
    }

    private Pair<Resource, String> generateFile(final Path root, final Path path, final Function<Path, File> mapper) {
        try {
            final Path absolute = root.resolve(path);
            final Path parent   = absolute.getParent();
            if (!parent.toFile().exists()) {
                final Path result = Files.createDirectories(parent);
                log.debug("Created parent folder '{}' for generated file '{}' under root folder '{}'", result, path.getFileName(), root);
            }
            final File   file = mapper.apply(path);
            final String hash = getFileDigest(file);
            log.debug("Added target path '{}' under root '{}', got file {} with MD5 checksum {}", path, root, file, hash);
            return new ImmutablePair<>(new FileSystemResource(file), hash);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private Path getWorkingFolder(final String folder) throws IOException {
        return Files.createTempDirectory(getWorkingFolder(), folder);
    }

    private Path getWorkingFolder() throws IOException {
        if (_workingDirectory == null || !_workingDirectory.toFile().exists()) {
            _workingDirectory = Files.createTempDirectory(XChangeStorageServiceTests.class.getSimpleName());
        }
        return _workingDirectory;
    }

    private static class FileGenerator implements Function<Path, File> {
        FileGenerator(final Path rootPath) {
            _rootPath = rootPath;
        }

        @Override
        public File apply(final Path path) {
            log.debug("Generating file \"{}\" under the root path \"{}\"", path, _rootPath);
            final File file = _rootPath.resolve(path).toFile();

            final String content;
            try (final PrintWriter writer = new PrintWriter(new FileWriter(file))) {
                final int minimum, maximum;
                switch (RandomUtils.nextInt(0, 5)) {
                    case 0:
                        minimum = RandomUtils.nextInt(3, 6);
                        maximum = minimum + RandomUtils.nextInt(1, 10);
                        content = LOREM.getHtmlParagraphs(minimum, maximum);
                        break;
                    case 1:
                        minimum = RandomUtils.nextInt(3, 6);
                        maximum = minimum + RandomUtils.nextInt(1, 10);
                        content = LOREM.getHtmlParagraphs(minimum, maximum);
                        break;
                    case 2:
                        minimum = RandomUtils.nextInt(3, 6);
                        maximum = minimum + RandomUtils.nextInt(1, 10);
                        content = LOREM.getHtmlParagraphs(minimum, maximum);
                        break;
                    case 3:
                        final StringWriter buffer = new StringWriter();
                        final PrintWriter print = new PrintWriter(buffer);
                        print.println(LOREM.getFirstName() + " " + LOREM.getLastName());
                        print.println(LOREM.getCity() + ", " + LOREM.getStateAbbr() + " " + LOREM.getZipCode());
                        print.println(LOREM.getCountry());
                        print.println(LOREM.getPhone());
                        print.println();
                        print.println(LOREM.getParagraphs(1, 2));
                        content = print.toString();
                        break;
                    default:
                        minimum = RandomUtils.nextInt(1000, 10000);
                        maximum = minimum + RandomUtils.nextInt(1000, 10000);
                        content = RandomStringUtils.randomGraph(minimum, maximum);

                        break;
                }
                writer.write(content);
                return file;
            } catch (FileNotFoundException e) {
                final Map<Path, Boolean> exists = checkHierarchyForExistence(_rootPath, convertPathToHierarchy(path));
                final StringWriter       string = new StringWriter();
                try (final PrintWriter writer = new PrintWriter(string)) {
                    writer.println("Got a file not found exception for path \"" + path + "\" under root path \"" + _rootPath + "\":");
                    writer.println(" * Root path exists? " + _rootPath.toFile().exists());
                    exists.keySet().stream().sorted().forEach(key -> writer.println(" * " + key + " exists? " + exists.get(key)));
                }
                throw new RuntimeException(string.toString(), e);
            } catch (IOException e) {
                throw new RuntimeException("An error occurred trying to write generated content to the path \"" + path + "\"", e);
            }
        }

        private final Path _rootPath;
    }

    @Test
    void testPathHierarchyDecomposition() {
        final Path one      = Paths.get("one");
        final Path two      = Paths.get("one", "two");
        final Path three    = Paths.get("one", "two", "three");
        final Path four     = Paths.get("one", "two", "three", "four");
        final Path fullPath = Paths.get("one", "two", "three", "four", "five");

        final List<Path> folders = atomize(fullPath);
        assertThat(folders).isNotNull().isNotEmpty().hasSize(5).containsExactly(one, Paths.get("two"), Paths.get("three"), Paths.get("four"), Paths.get("five"));

        final List<Path> hierarchy = convertPathToHierarchy(fullPath);
        assertThat(hierarchy).isNotNull().isNotEmpty().hasSize(5).containsExactly(one, two, three, four, fullPath);

        final Map<Path, Boolean> paths = checkHierarchyForExistence(hierarchy);
        assertThat(paths).isNotNull().isNotEmpty().hasSize(folders.size()).containsOnly(entry(one, false), entry(two, false), entry(three, false), entry(four, false), entry(fullPath, false));
    }

    private static final ResourceManager RESOURCE_MANAGER = ResourceManager.getInstance();
    private static final Path            FILE_LIST        = RESOURCE_MANAGER.getTestResourceFile("files.txt").toPath();
    private static final List<Path>      RESOURCES        = Arrays.asList(Paths.get("files/resource1.txt"), Paths.get("files/resource2.txt"), Paths.get("files/resource3.txt"));
    private static final List<String>    EXTENSIONS       = Arrays.asList("annot", "area", "bak", "cmd", "ctab", "dat", "dcm", "done", "env", "err", "gif", "h", "hdr", "ifh", "img", "jpg", "label", "log", "lta", "m3z", "mdl", "mgh", "mgz", "png", "rec", "stats", "suvr", "tac", "touch", "txt", "xfm", "zip");
    private static final Lorem           LOREM            = LoremIpsum.getInstance();

    @Autowired
    private XChangeProperties _properties;

    @Autowired
    private XChangeStorageService _service;

    private Path _workingDirectory;
}
