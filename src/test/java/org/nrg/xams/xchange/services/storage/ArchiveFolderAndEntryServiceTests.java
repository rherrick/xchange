package org.nrg.xams.xchange.services.storage;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.nrg.xams.xchange.services.storage.domain.auditing.EntityHistory;
import org.nrg.xams.xchange.services.storage.domain.entities.ArchiveEntry;
import org.nrg.xams.xchange.services.storage.domain.entities.ArchiveFolder;
import org.nrg.xams.xchange.services.storage.domain.services.ArchiveEntryService;
import org.nrg.xams.xchange.services.storage.domain.services.ArchiveFolderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.nio.file.Path;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("postgresql")
@ContextConfiguration(classes = StorageServiceTestsConfig.class)
@ComponentScan("org.nrg.xams.xchange.services.storage.domain")
@Slf4j
class ArchiveFolderAndEntryServiceTests {
    @Test
    void contextLoads() {
    }

    @Test
    void testSimpleArchiveFolderAndEntries() {
        // Create archiveFolder1 with implicit constructor in create()
        final ArchiveFolder archiveFolder1 = _archiveFolders.create("ArchiveFolder 1", "archiveFolder1");

        // Create archiveFolder2 with explicit object creation
        final ArchiveFolder archiveFolder2 = new ArchiveFolder();
        archiveFolder2.setName("ArchiveFolder 2");
        archiveFolder2.setPath("archiveFolder2");
        _archiveFolders.create(archiveFolder2);

        // Create archiveFolder3 with explicit object creation
        final ArchiveFolder archiveFolder3 = new ArchiveFolder("ArchiveFolder 3", "archiveFolder3");
        _archiveFolders.create(archiveFolder3);

        final ArchiveFolder retrievedByName1 = _archiveFolders.findByName("ArchiveFolder 1");
        final ArchiveFolder retrievedByPath1 = _archiveFolders.findByPath("archiveFolder1");
        final ArchiveFolder retrievedByName2 = _archiveFolders.findByName("ArchiveFolder 2");
        final ArchiveFolder retrievedByPath2 = _archiveFolders.findByPath("archiveFolder2");
        final ArchiveFolder retrievedByName3 = _archiveFolders.findByName("ArchiveFolder 3");
        final ArchiveFolder retrievedByPath3 = _archiveFolders.findByPath("archiveFolder3");

        assertThat(retrievedByName1).isNotNull();
        assertThat(retrievedByName1.getName()).isEqualTo(archiveFolder1.getName());
        assertThat(retrievedByName1.getPath()).isEqualTo(archiveFolder1.getPath());
        assertThat(retrievedByPath1).isNotNull();
        assertThat(retrievedByPath1.getName()).isEqualTo(archiveFolder1.getName());
        assertThat(retrievedByPath1.getPath()).isEqualTo(archiveFolder1.getPath());
        assertThat(retrievedByName2).isNotNull();
        assertThat(retrievedByName2.getName()).isEqualTo(archiveFolder2.getName());
        assertThat(retrievedByName2.getPath()).isEqualTo(archiveFolder2.getPath());
        assertThat(retrievedByPath2).isNotNull();
        assertThat(retrievedByPath2.getName()).isEqualTo(archiveFolder2.getName());
        assertThat(retrievedByPath2.getPath()).isEqualTo(archiveFolder2.getPath());
        assertThat(retrievedByName3).isNotNull();
        assertThat(retrievedByName3.getName()).isEqualTo(archiveFolder3.getName());
        assertThat(retrievedByName3.getPath()).isEqualTo(archiveFolder3.getPath());
        assertThat(retrievedByPath3).isNotNull();
        assertThat(retrievedByPath3.getName()).isEqualTo(archiveFolder3.getName());
        assertThat(retrievedByPath3.getPath()).isEqualTo(archiveFolder3.getPath());

        final ArchiveEntry entry11 = _entries.create(archiveFolder1, "archiveFolder1", "entry1", "945377538db599149857cc5b8302938c");

        final ArchiveEntry entry12 = new ArchiveEntry();
        entry12.setArchiveFolder(archiveFolder1);
        entry12.setPath("archiveFolder1");
        entry12.setName("entry2");
        entry12.setDigest("d9bb9f968bb67acfda5c9160ac7c8f44");
        _entries.create(entry12);

        final ArchiveEntry entry13 = new ArchiveEntry(archiveFolder1, "archiveFolder1", "entry3", "2676acbe8792ae9e5b6cafb2fa7d8885");
        _entries.create(entry13);

        _archiveFolders.refresh(retrievedByName1, retrievedByName2, retrievedByName3);
        final Set<ArchiveEntry> entries = retrievedByName1.getEntries();
        assertThat(entries).isNotNull();
        // Assertions.
        assertThat(entries.contains(entry11)).isTrue();
        assertThat(entries.contains(entry12)).isTrue();
        assertThat(entries.contains(entry13)).isTrue();
    }

    @Test
    void testObjectVersioning() {
        final ArchiveFolder archiveFolder1 = new ArchiveFolder("Versioned ArchiveFolder 1", "versioned1",
                                                               new ArchiveEntry("Versioned ArchiveFolder 1 Entry 1", "versioned1-1", FIRST_DIGEST),
                                                               new ArchiveEntry("Versioned ArchiveFolder 1 Entry 2", "versioned1-2", FIRST_DIGEST),
                                                               new ArchiveEntry("Versioned ArchiveFolder 1 Entry 3", "versioned1-3", FIRST_DIGEST));
        _archiveFolders.create(archiveFolder1);

        archiveFolder1.getEntries().forEach(entry -> entry.setDigest(SECOND_DIGEST));
        _archiveFolders.update(archiveFolder1);

        final Iterator<ArchiveEntry> iterator = archiveFolder1.getEntries().iterator();
        final ArchiveEntry           entry1   = iterator.next();
        final ArchiveEntry           entry2   = iterator.next();
        final ArchiveEntry           entry3   = iterator.next();

        final ArchiveFolder     retrieved = _archiveFolders.findByName("Versioned ArchiveFolder 1");
        final Set<ArchiveEntry> entries   = retrieved.getEntries();
        final Map<String, ArchiveEntry> mappedEntries = entries.stream().collect(Collectors.toMap(ArchiveEntry::getName, Function.identity()));
        assertThat(entry1).isNotNull().isEqualTo(mappedEntries.get(entry1.getName()));
        assertThat(entry2).isNotNull().isEqualTo(mappedEntries.get(entry2.getName()));
        assertThat(entry3).isNotNull().isEqualTo(mappedEntries.get(entry3.getName()));
        assertThat(entries).isNotNull().isNotEmpty().hasSize(3).containsExactlyInAnyOrder(entry1, entry2, entry3);

        final List<EntityHistory<ArchiveFolder>>           archiveFolderHistory   = _archiveFolders.getHistory(retrieved.getId());
        final List<Number>                                 archiveFolderRevisions = _archiveFolders.getRevisions(retrieved.getId());
        final Map<Path, List<EntityHistory<ArchiveEntry>>> entriesHistory         = _entries.getHistory(retrieved);
        final Map<Path, List<Number>>                      entryRevisions         = _entries.getRevisions(retrieved);

        assertThat(archiveFolderHistory).isNotNull().isNotEmpty();
        final Set<ArchiveEntry> historyEntries = archiveFolderHistory.get(0).getEntity().getEntries();
        log.warn("History entries count: {}", historyEntries.size());
        assertThat(historyEntries).isNotNull().isNotEmpty().hasSize(entries.size());
        assertThat(entriesHistory).isNotNull().isNotEmpty();

        final List<Number> entry1Revisions = _entries.getRevisions(entry1.getId());
        assertThat(entry1Revisions).isNotNull().isNotEmpty().hasSize(2);

        // Different versions have different values that we know changed, but the ID should be the same.
        final ArchiveEntry revision11 = _entries.getRevision(entry1.getId(), 1);
        assertThat(revision11).isNotNull().hasFieldOrPropertyWithValue("id", entry1.getId()).hasFieldOrPropertyWithValue("digest", FIRST_DIGEST);
        final ArchiveEntry revision12 = _entries.getRevision(entry1.getId(), 2);
        assertThat(revision12).isNotNull().hasFieldOrPropertyWithValue("id", entry1.getId()).hasFieldOrPropertyWithValue("digest", SECOND_DIGEST);
        final ArchiveEntry revision21 = _entries.getRevision(entry2.getId(), 1);
        assertThat(revision21).isNotNull().hasFieldOrPropertyWithValue("id", entry2.getId()).hasFieldOrPropertyWithValue("digest", FIRST_DIGEST);
        final ArchiveEntry revision22 = _entries.getRevision(entry2.getId(), 2);
        assertThat(revision22).isNotNull().hasFieldOrPropertyWithValue("id", entry2.getId()).hasFieldOrPropertyWithValue("digest", SECOND_DIGEST);
        final ArchiveEntry revision31 = _entries.getRevision(entry3.getId(), 1);
        assertThat(revision31).isNotNull().hasFieldOrPropertyWithValue("id", entry3.getId()).hasFieldOrPropertyWithValue("digest", FIRST_DIGEST);
        final ArchiveEntry revision32 = _entries.getRevision(entry3.getId(), 2);
        assertThat(revision32).isNotNull().hasFieldOrPropertyWithValue("id", entry3.getId()).hasFieldOrPropertyWithValue("digest", SECOND_DIGEST);
    }

    private static final String FIRST_DIGEST  = "first";
    private static final String SECOND_DIGEST = "second";

    @Autowired
    private ArchiveFolderService _archiveFolders;
    @Autowired
    private ArchiveEntryService  _entries;
}
