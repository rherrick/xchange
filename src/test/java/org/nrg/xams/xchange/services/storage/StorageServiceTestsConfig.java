package org.nrg.xams.xchange.services.storage;

import org.hibernate.SessionFactory;
import org.hibernate.cache.ehcache.SingletonEhCacheRegionFactory;
import org.hibernate.cache.spi.RegionFactory;
import org.hibernate.cfg.ImprovedNamingStrategy;
import org.nrg.framework.orm.hibernate.AggregatedAnnotationSessionFactoryBean;
import org.nrg.framework.orm.hibernate.PrefixedTableNamingStrategy;
import org.nrg.xams.xchange.config.XChangeServicesConfig;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.support.ResourceTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

import javax.sql.DataSource;
import java.sql.Driver;
import java.util.Properties;

@Configuration
@PropertySource(value = "classpath:storage-test-${envTarget:default}.properties")
@Import(XChangeServicesConfig.class)
@EnableTransactionManagement
class StorageServiceTestsConfig {
    @Bean
    public DataSource dataSource() throws ClassNotFoundException {
        final SimpleDriverDataSource dataSource = new SimpleDriverDataSource();
        dataSource.setDriverClass(Class.forName(_databaseDriver).asSubclass(Driver.class));
        dataSource.setUrl(_databaseUrl);
        dataSource.setUsername(_databaseUser);
        dataSource.setPassword(_databasePass);
        return dataSource;
    }

    @Bean
    public ImprovedNamingStrategy namingStrategy() {
        return new PrefixedTableNamingStrategy("xhbm");
    }

    @Bean
    public PropertiesFactoryBean hibernateProperties() {
        final Properties properties = new Properties();
        properties.setProperty("hibernate.dialect", _hibernateDialect);
        properties.setProperty("hibernate.hbm2ddl.auto", _hibernateHbm2ddlAuto);
        properties.setProperty("hibernate.show_sql", _hibernateShowSql);
        properties.setProperty("hibernate.cache.use_second_level_cache", _hibernateUseSecondLevelCache);
        properties.setProperty("hibernate.cache.use_query_cache", _hibernateUseQueryCache);

        final PropertiesFactoryBean bean = new PropertiesFactoryBean();
        bean.setProperties(properties);
        return bean;
    }

    @Bean
    public RegionFactory regionFactory(@Qualifier("hibernateProperties") final Properties properties) {
        return new SingletonEhCacheRegionFactory(properties);
    }

    @Bean
    public FactoryBean<SessionFactory> sessionFactory(final RegionFactory factory,
                                                      final DataSource dataSource,
                                                      @Qualifier("hibernateProperties") final Properties properties,
                                                      final ImprovedNamingStrategy namingStrategy) {
        final AggregatedAnnotationSessionFactoryBean bean = new AggregatedAnnotationSessionFactoryBean();
        bean.setCacheRegionFactory(factory);
        bean.setDataSource(dataSource);
        bean.setHibernateProperties(properties);
        bean.setNamingStrategy(namingStrategy);
        return bean;
    }

    @Bean
    public ResourceTransactionManager transactionManager(final FactoryBean<SessionFactory> sessionFactory) throws Exception {
        return new HibernateTransactionManager(sessionFactory.getObject());
    }

    @Bean
    public TransactionTemplate transactionTemplate(final PlatformTransactionManager transactionManager) {
        return new TransactionTemplate(transactionManager);
    }

    @Bean
    public JdbcTemplate jdbcTemplate() throws ClassNotFoundException {
        return new JdbcTemplate(dataSource());
    }

    @Value("${database.driver:org.h2.Driver}")
    private String _databaseDriver;

    @Value("${database.url:jdbc:h2:mem:test;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE}")
    private String _databaseUrl;

    @Value("${database.username:sa}")
    private String _databaseUser;

    @Value("${database.password}")
    private String _databasePass;

    @Value("${hibernate.dialect:org.hibernate.dialect.H2Dialect}")
    private String _hibernateDialect;

    @Value("${hibernate.hbm2ddl.auto:create-drop}")
    private String _hibernateHbm2ddlAuto;

    @Value("${hibernate.show_sql:false}")
    private String _hibernateShowSql;

    @Value("${hibernate.cache.use_second_level_cache:true}")
    private String _hibernateUseSecondLevelCache;

    @Value("${hibernate.cache.use_query_cache:true}")
    private String _hibernateUseQueryCache;
}

