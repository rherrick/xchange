package org.nrg.xams.xchange.services.storage.domain.repositories;

import org.nrg.xams.xchange.services.storage.domain.entities.ArchiveFolder;
import org.springframework.stereotype.Repository;

import java.nio.file.Path;

@Repository
public class ArchiveFolderRepository extends AbstractEnversHibernateDAO<ArchiveFolder> {
    public boolean exists(final String name) {
        return exists("name", name);
    }

    public boolean exists(final Path path) {
        return exists("path", path.toString());
    }
}
