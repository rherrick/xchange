package org.nrg.xams.xchange.services.storage.impl.jcr;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.nrg.xams.xchange.services.storage.XChangeProperties;
import org.nrg.xams.xchange.services.storage.XChangeVersionStore;
import org.nrg.xams.xchange.services.storage.domain.auditing.EntityHistory;
import org.nrg.xams.xchange.services.storage.domain.entities.ArchiveEntry;
import org.nrg.xams.xchange.services.storage.domain.entities.ArchiveFolder;
import org.nrg.xams.xchange.services.storage.domain.services.ArchiveEntryService;
import org.nrg.xams.xchange.services.storage.domain.services.ArchiveFolderService;
import org.nrg.xams.xchange.services.storage.impl.AbstractXChangeStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

@Service
@Profile("jcr")
@Lazy
@Slf4j
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(prefix = "_")
public class JcrXChangeStorageService extends AbstractXChangeStorageService {
    @Autowired
    public JcrXChangeStorageService(final XChangeProperties properties, final ArchiveFolderService archiveFolderService, final ArchiveEntryService archiveEntryService, final XChangeVersionStore versionStore) {
        super(properties, archiveFolderService, archiveEntryService, versionStore);
    }

    @PostConstruct
    @Override
    public void init() {

    }

    @Override
    public ArchiveFolder store(final String name, final Path rootPath) {
        return null;
    }

    @Override
    public ArchiveFolder store(final String name, final Path rootPath, final Map<Path, Resource> entries) {
        return null;
    }

    @Override
    public ArchiveFolder store(final Path rootPath, final Map<Path, Resource> entries) {
        return null;
    }

    @Override
    public ArchiveFolder store(final ArchiveFolder archiveFolder, final Map<Path, Resource> entries) {
        return null;
    }

    @Override
    public ArchiveFolder delete(final ArchiveFolder archiveFolder, final List<Path> entries) {
        return null;
    }

    @Override
    public ArchiveFolder getArchiveFolder(final Path path) {
        return null;
    }

    @Override
    public ArchiveFolder getArchiveFolder(final Path path, final Number revision) {
        return null;
    }

    @Override
    public List<Number> getRevisions(final ArchiveFolder archiveFolder) {
        return null;
    }

    @Override
    public List<EntityHistory<ArchiveFolder>> getHistory(final ArchiveFolder archiveFolder) {
        return null;
    }

    @Override
    public ArchiveEntry getEntry(final Path archiveEntryPath, final Path entryPath) {
        return null;
    }

    @Override
    public ArchiveEntry getEntry(final Path archiveEntryPath, final Path entryPath, final Number revision) {
        return null;
    }

    @Override
    public List<Number> getRevisions(final ArchiveEntry entry) {
        return null;
    }

    @Override
    public List<EntityHistory<ArchiveEntry>> getHistory(final ArchiveEntry entry) {
        return null;
    }

    @Override
    public Resource getResource(final ArchiveEntry entry) {
        return null;
    }

    @Override
    public Resource getResource(final ArchiveEntry entry, final Number revision) {
        return null;
    }

    @Override
    public Map<Path, Resource> getResources(final ArchiveFolder archiveFolder) {
        return null;
    }

    @Override
    public Map<Path, Resource> getResources(final ArchiveFolder archiveFolder, final Number revision) {
        return null;
    }

    @Override
    public void store(final MultipartFile file) {

    }

    @Override
    public void store(final MultipartFile file, final Path path) {

    }

    @Override
    public Stream<Path> loadAll() {
        return null;
    }

    @Override
    public Path load(final String filename) {
        return null;
    }

    @Override
    public Resource loadAsResource(final String filename) {
        return null;
    }

    @Override
    public void deleteAll() {

    }
}
