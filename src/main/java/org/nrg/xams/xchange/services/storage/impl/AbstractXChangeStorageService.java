package org.nrg.xams.xchange.services.storage.impl;

import lombok.Getter;
import lombok.experimental.Accessors;
import org.nrg.xams.xchange.services.storage.XChangeProperties;
import org.nrg.xams.xchange.services.storage.XChangeStorageService;
import org.nrg.xams.xchange.services.storage.XChangeVersionStore;
import org.nrg.xams.xchange.services.storage.domain.services.ArchiveEntryService;
import org.nrg.xams.xchange.services.storage.domain.services.ArchiveFolderService;

import static lombok.AccessLevel.PROTECTED;

@Getter(PROTECTED)
@Accessors(prefix = "_")
public abstract class AbstractXChangeStorageService implements XChangeStorageService {
    protected AbstractXChangeStorageService(final XChangeProperties properties, final ArchiveFolderService archiveFolderService, final ArchiveEntryService archiveEntryService, final XChangeVersionStore versionStore) {
        _properties = properties;
        _archiveFolderService = archiveFolderService;
        _archiveEntryService = archiveEntryService;
        _versionStore = versionStore;
    }

    private final XChangeProperties    _properties;
    private final ArchiveFolderService _archiveFolderService;
    private final ArchiveEntryService  _archiveEntryService;
    private final XChangeVersionStore  _versionStore;
}
