package org.nrg.xams.xchange.services.storage.exceptions;

public class FileNotFoundException extends XChangeStorageServiceException {
    public FileNotFoundException(final String message) {
        super(message);
    }

    public FileNotFoundException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
