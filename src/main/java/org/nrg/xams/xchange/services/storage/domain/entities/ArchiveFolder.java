package org.nrg.xams.xchange.services.storage.domain.entities;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Proxy;
import org.hibernate.envers.Audited;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntity;
import org.nrg.xams.xchange.services.storage.domain.auditing.XChangeRevisionListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.EAGER;

/**
 * Represents a archive folder, which is a set of files that form a cohesive entity.
 */
@Entity
@Audited
@EntityListeners(XChangeRevisionListener.class)
@Proxy(lazy = false)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "nrg")
@Slf4j
public class ArchiveFolder extends AbstractHibernateEntity {
    /**
     * Creates a new archive folder with no properties set.
     */
    public ArchiveFolder() {
        log.debug("Creating a new default archive folder");
    }

    /**
     * Creates a new archive folder with the specified {@link #getName() name} and {@link #getPath() path}.
     *
     * @param name The name of the archive folder.
     * @param path The path for the archive folder.
     */
    public ArchiveFolder(final String name, final Path path) {
        this(name, path.toString());
    }

    /**
     * Creates a new archive folder with the specified {@link #getName() name} and {@link #getPath() path}.
     *
     * @param name The name of the archive folder.
     * @param path The path for the archive folder.
     */
    public ArchiveFolder(final String name, final String path) {
        this(name, path, EMPTY_ENTRY_ARRAY);
    }

    public ArchiveFolder(final String name, final String path, final ArchiveEntry... entries) {
        log.debug("Creating a new archive folder '{}' with path '{}'", name, path);
        _name = name;
        _path = path;
        if (entries.length > 0) {
            setEntries(new HashSet<>(Arrays.asList(entries)));
        }
    }

    /**
     * The name of the resource represented by the archive folder.
     *
     * @return The name of the archive folder.
     */
    @NotNull
    @Column(unique = true, nullable = false)
    public String getName() {
        return _name;
    }

    /**
     * Sets the name for the archive folder.
     *
     * @param name The name to set for the archive folder.
     */
    public void setName(final String name) {
        _name = name;
    }

    /**
     * The path to the archive folder within the scope of the storage service.
     *
     * @return Returns the path of the archive folder.
     */
    @NotNull
    @Column(unique = true, nullable = false)
    public String getPath() {
        return _path;
    }

    /**
     * Sets the archive folder's path to the submitted value.
     *
     * @param path The string representation of the path to set.
     */
    public void setPath(final String path) {
        _path = path;
    }

    /**
     * Sets the archive folder's path to the submitted value.
     *
     * @param path The path to set.
     */
    @Transient
    public void setPath(final Path path) {
        setPath(path.toString());
    }

    /**
     * The UID of the archive folder if applicable.
     *
     * @return Returns the UID for the archive folder.
     */
    // TODO: Convert this to an externalizable properties sheet.
    public String getUid() {
        return _uid;
    }

    /**
     * Sets the UID for this archive folder.
     *
     * @param uid The UID to set.
     */
    public void setUid(final String uid) {
        _uid = uid;
    }

    /**
     * Indicates the {@link ArchiveEntry entries} in this archive folder.
     *
     * @return The entries in this archive folder.
     */
    @OneToMany(targetEntity = ArchiveEntry.class, fetch = EAGER, mappedBy = "archiveFolder", cascade = ALL, orphanRemoval = true)
    @JsonManagedReference
    public Set<ArchiveEntry> getEntries() {
        return _entries;
    }

    /**
     * Sets the {@link ArchiveEntry entries} in this archive folder.
     *
     * @param entries The entries to set for this archive folder.
     */
    public void setEntries(final Set<ArchiveEntry> entries) {
        _entries = entries;
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof ArchiveFolder)) {
            return false;
        }
        // TODO: There's a bug where the updated timestamp is set to the current time instead of the last update time of the database record, so don't use this for now.
        // if (!super.equals(other)) {
        //     return false;
        // }
        final ArchiveFolder archiveFolder = (ArchiveFolder) other;
        return Objects.equals(getId(), archiveFolder.getId()) &&
               Objects.equals(getPath(), archiveFolder.getPath()) &&
               Objects.equals(getName(), archiveFolder.getName()) &&
               Objects.equals(getUid(), archiveFolder.getUid());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getPath(), getName(), getUid());
    }

    @Override
    public String toString() {
        return "Archive folder " + _name + " (" + getId() + ") path='" + _path + '\'' + (StringUtils.isNotBlank(_uid) ? ", uid='" + _uid + '\'' : "");
    }

    /**
     * Gets the {@link #getPath() path} of the archive folder, transformed into an actual Java <b>Path</b> object.
     *
     * @return The path for the archive folder.
     */
    @Transient
    public Path getPathAsPath() {
        return Paths.get(getPath());
    }

    @Transient
    public void addEntry(final ArchiveEntry entry) {
        if (_entries == null) {
            _entries = new HashSet<>();
        }
        entry.setArchiveFolder(this);
        _entries.add(entry);
    }

    private static final ArchiveEntry[] EMPTY_ENTRY_ARRAY = new ArchiveEntry[0];

    private String            _name;
    private String            _path;
    private String            _uid;
    private Set<ArchiveEntry> _entries;
}
