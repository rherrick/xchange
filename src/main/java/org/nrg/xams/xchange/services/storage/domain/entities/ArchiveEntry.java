package org.nrg.xams.xchange.services.storage.domain.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.Proxy;
import org.hibernate.envers.Audited;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntity;
import org.nrg.xams.xchange.services.storage.domain.auditing.XChangeRevisionListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static javax.persistence.FetchType.LAZY;
import static org.hibernate.annotations.CacheConcurrencyStrategy.READ_WRITE;

/**
 * Represents a single entry&mdash;basically a file&mdash;in an {@link ArchiveFolder archive folder}.
 */
@Entity
@Audited
@EntityListeners(XChangeRevisionListener.class)
@Proxy(lazy = false)
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"archive_folder", "path", "name"}))
@Cache(usage = READ_WRITE, region = "nrg")
@Slf4j
public class ArchiveEntry extends AbstractHibernateEntity {
    /**
     * Creates a new entry with no properties set.
     */
    public ArchiveEntry() {
        log.debug("Creating a new default archive entry");
    }

    /**
     * Creates a new entry with the indicated properties.
     *
     * @param path   The string representation of the path to set.
     * @param name   The name to set for the entry.
     * @param digest The digest to set for the entry.
     */
    public ArchiveEntry(final Path path, final String name, final String digest) {
        this(path.toString(), name, digest);
    }

    /**
     * Creates a new entry with the indicated properties.
     *
     * @param path   The string representation of the path to set.
     * @param name   The name to set for the entry.
     * @param digest The digest to set for the entry.
     */
    public ArchiveEntry(final String path, final String name, final String digest) {
        log.debug("Creating a new entry '{}' with path '{}' and digest '{}' without an archive folder", name, path, digest);
        setPath(path);
        setName(name);
        setDigest(digest);
    }

    /**
     * Creates a new entry with the indicated properties.
     *
     * @param archiveFolder The archive folder to set for the entry.
     * @param path          The string representation of the path to set.
     * @param name          The name to set for the entry.
     * @param digest        The digest to set for the entry.
     */
    public ArchiveEntry(final ArchiveFolder archiveFolder, final Path path, final String name, final String digest) {
        this(archiveFolder, path.toString(), name, digest);
    }

    /**
     * Creates a new entry with the indicated properties.
     *
     * @param archiveFolder The archive folder to set for the entry.
     * @param path          The string representation of the path to set.
     * @param name          The name to set for the entry.
     * @param digest        The digest to set for the entry.
     */
    public ArchiveEntry(final ArchiveFolder archiveFolder, final String path, final String name, final String digest) {
        log.debug("Creating a new entry '{}' in archive folder '{}' with path '{}' and digest '{}'", name, archiveFolder.getName(), path, digest);
        setArchiveFolder(archiveFolder);
        setPath(path);
        setName(name);
        setDigest(digest);
    }

    /**
     * The {@link ArchiveFolder archive folder} that contains this entry.
     *
     * @return The associated {@link ArchiveFolder} instance.
     */
    @NotNull
    @ManyToOne(fetch = LAZY)
    @JsonBackReference
    public ArchiveFolder getArchiveFolder() {
        return _archiveFolder;
    }

    /**
     * Sets the archive folder for this entry.
     *
     * @param archiveFolder The archive folder to set for the entry.
     */
    public void setArchiveFolder(final ArchiveFolder archiveFolder) {
        _archiveFolder = archiveFolder;
    }

    /**
     * The path to the entry within the scope of its archive folder. Note that
     * the path does <i>not</i> include the entry name, so multiple entries
     * may have the same path. The combination of {@link ArchiveFolder archive folder},
     * path, and {@link #getName() name} must be unique.
     *
     * @return The path to the entry.
     *
     * @see #getPathAsPath()
     */
    @NotNull
    @Column(name = "path", nullable = false)
    public String getPath() {
        return _path;
    }

    /**
     * Sets the archive folder's path to the submitted value.
     *
     * @param path The string representation of the path to set.
     */
    public void setPath(final String path) {
        _path = path;
    }

    /**
     * Sets the archive folder's path to the submitted value.
     *
     * @param path The path to set.
     */
    @Transient
    public void setPath(final Path path) {
        setPath(path.toString());
    }

    /**
     * The name of the resource represented by the entry. Note that the name
     * does not include the {@link #getPath() entry path}, so multiple entries
     * may have the same name. The combination of {@link ArchiveFolder archive
     * folder}, {@link #getPath() path}, and name must be unique.
     *
     * @return The name of the entry.
     */
    @NotNull
    @Column(nullable = false)
    public String getName() {
        return _name;
    }

    /**
     * Sets the name for the entry.
     *
     * @param name The name to set for the entry.
     */
    public void setName(final String name) {
        _name = name;
    }

    /**
     * The digest or checksum of the entry data.
     *
     * @return The digest for the entry.
     */
    @NotNull
    @Column(nullable = false)
    public String getDigest() {
        return _digest;
    }

    /**
     * Sets the digest value for the entry.
     *
     * @param digest The digest to set for the entry.
     */
    public void setDigest(final String digest) {
        _digest = digest;
    }

    /**
     * The UID of the entry if applicable.
     *
     * @return Returns the UID for the entry.
     */
    // TODO: Convert this to an externalizable properties sheet.
    public String getUid() {
        return _uid;
    }

    /**
     * Sets the UID for the entry.
     *
     * @param uid The UID for the entry.
     */
    public void setUid(final String uid) {
        _uid = uid;
    }

    /**
     * The instance number of the entry if applicable.
     *
     * @return The instance number of the entry.
     */
    // TODO: Convert this to an externalizable properties sheet.
    public String getInstanceNumber() {
        return _instanceNumber;
    }

    /**
     * Sets the instance number for the entry.
     *
     * @param instanceNumber The instance number to set for the entry.
     */
    public void setInstanceNumber(final String instanceNumber) {
        _instanceNumber = instanceNumber;
    }

    /**
     * The content label for the entry.
     *
     * @return The content label of the entry.
     */
    public String getContent() {
        return _content;
    }

    /**
     * Sets the content label for the entry.
     *
     * @param content The content label to set for the entry.
     */
    public void setContent(final String content) {
        _content = content;
    }

    /**
     * The format label for the entry.
     *
     * @return The format label of the entry.
     */
    public String getFormat() {
        return _format;
    }

    /**
     * Sets the format label for the entry.
     *
     * @param format The format label to set for the entry.
     */
    public void setFormat(final String format) {
        _format = format;
    }

    @ElementCollection(targetClass = String.class)
    public List<String> getArchiveIds() {
        return _archiveIds;
    }

    public void setArchiveIds(final List<String> archiveIds) {

    }

    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof ArchiveEntry)) {
            return false;
        }
        // TODO: There's a bug where the updated timestamp is set to the current time instead of the last update time of the database record, so don't use this for now.
        // if (!super.equals(other)) {
        //    return false;
        // }
        final ArchiveEntry entry = (ArchiveEntry) other;
        return Objects.equals(getArchiveFolder(), entry.getArchiveFolder()) &&
               Objects.equals(getId(), entry.getId()) &&
               Objects.equals(getPath(), entry.getPath()) &&
               Objects.equals(getName(), entry.getName()) &&
               Objects.equals(getDigest(), entry.getDigest()) &&
               Objects.equals(getUid(), entry.getUid()) &&
               Objects.equals(getInstanceNumber(), entry.getInstanceNumber()) &&
               Objects.equals(getContent(), entry.getContent()) &&
               Objects.equals(getFormat(), entry.getFormat());
    }

    @Override
    public int hashCode() {
        final long catalogId = getArchiveFolder() != null ? getArchiveFolder().getId() : 0;
        return Objects.hash(getId(), catalogId, getPath(), getName(), getDigest(), getUid(), getInstanceNumber(), getContent(), getFormat());
    }

    @Override
    public String toString() {
        final String uid               = StringUtils.isNotBlank(_uid) ? ", uid='" + _uid + '\'' : "";
        final String instanceNumber    = StringUtils.isNotBlank(_instanceNumber) ? ", instanceNumber='" + _instanceNumber + '\'' : "";
        final String content           = StringUtils.isNotBlank(_content) ? ", content='" + _content + '\'' : "";
        final String format            = StringUtils.isNotBlank(_format) ? ", format='" + _format + '\'' : "";
        final String archiveFolderName = _archiveFolder != null ? _archiveFolder.getName() : "<null>";
        final long   archiveFolderId   = _archiveFolder != null ? _archiveFolder.getId() : 0;
        return "Entry " + getId() + " in archive folder " + archiveFolderName + " (" + archiveFolderId +
               ", path='" + _path + '\'' + ", name='" + _name + '\'' + ", digest='" + _digest + '\'' +
               uid + instanceNumber + content + format;
    }

    /**
     * Gets the {@link #getPath() path} of the entry, transformed into an actual Java <b>Path</b> object.
     *
     * @return The path for the entry.
     */
    @Transient
    public Path getPathAsPath() {
        return Paths.get(getPath());
    }

    @Transient
    public void addArchiveId(final String archiveId) {
        _archiveIds.add(archiveId);
    }

    private ArchiveFolder _archiveFolder;
    private String        _path;
    private String        _name;
    private String        _digest;
    private String        _uid;
    private String        _instanceNumber;
    private String        _content;
    private String        _format;

    private final List<String> _archiveIds = new ArrayList<>();
}
