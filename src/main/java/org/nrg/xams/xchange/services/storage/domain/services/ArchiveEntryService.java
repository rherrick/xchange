package org.nrg.xams.xchange.services.storage.domain.services;

import org.nrg.xams.xchange.services.storage.domain.auditing.EntityHistory;
import org.nrg.xams.xchange.services.storage.domain.entities.ArchiveEntry;
import org.nrg.xams.xchange.services.storage.domain.entities.ArchiveFolder;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

public interface ArchiveEntryService extends EnversHibernateEntityService<ArchiveEntry> {
    ArchiveEntry findByName(final String name);

    ArchiveEntry findByPath(final Path path);

    ArchiveEntry findByPath(final String path);

    InputStream getEntry(final ArchiveEntry entry) throws IOException;

    InputStream getEntry(final ArchiveEntry entry, final int version) throws IOException;

    void delete(final List<ArchiveEntry> entries);

    Map<Path, List<EntityHistory<ArchiveEntry>>> getHistory(final ArchiveFolder archiveFolder);

    Map<Path, List<Number>> getRevisions(final ArchiveFolder archiveFolder);
}
