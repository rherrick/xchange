package org.nrg.xams.xchange.services.storage.domain.services.impl;

import org.nrg.xams.xchange.services.storage.domain.entities.ArchiveEntry;
import org.nrg.xams.xchange.services.storage.domain.entities.ArchiveFolder;
import org.nrg.xams.xchange.services.storage.domain.repositories.ArchiveFolderRepository;
import org.nrg.xams.xchange.services.storage.domain.services.ArchiveFolderService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.nio.file.Path;
import java.util.Set;

@Service
@Transactional
public class HibernateArchiveFolderService extends AbstractEnversHibernateEntityService<ArchiveFolder, ArchiveFolderRepository> implements ArchiveFolderService {
    @Override
    public ArchiveFolder create(final ArchiveFolder archiveFolder) {
        final Set<ArchiveEntry> entries = archiveFolder.getEntries();
        if (entries != null) {
            entries.forEach(entry -> entry.setArchiveFolder(archiveFolder));
        }
        return super.create(archiveFolder);
    }

    @Override
    public boolean exists(final String name) {
        return getEnversDao().exists(name);
    }

    @Override
    public boolean exists(final Path path) {
        return getEnversDao().exists(path);
    }
}
