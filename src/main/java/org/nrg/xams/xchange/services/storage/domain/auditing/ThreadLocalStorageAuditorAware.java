package org.nrg.xams.xchange.services.storage.domain.auditing;

import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;

import static org.springframework.web.context.request.RequestAttributes.SCOPE_REQUEST;

@Component
public class ThreadLocalStorageAuditorAware { // implements AuditorAware<String> {
    // @Override
    public String getCurrentAuditor() {
        return (String) RequestContextHolder
                .currentRequestAttributes()
                .getAttribute("Username", SCOPE_REQUEST);
    }

}
