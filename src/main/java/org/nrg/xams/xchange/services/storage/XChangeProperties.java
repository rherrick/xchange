package org.nrg.xams.xchange.services.storage;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.beans.factory.annotation.Value;

@Data
@Accessors(prefix = "_")
public class XChangeProperties {
    @Value("${storage.location:xchange-uploads}")
    private String _storageLocation;
}
