package org.nrg.xams.xchange.services.storage.domain.repositories;

import org.nrg.xams.xchange.services.storage.domain.entities.ArchiveEntry;
import org.springframework.stereotype.Repository;

@Repository
public class ArchiveEntryRepository extends AbstractEnversHibernateDAO<ArchiveEntry> {
}
