package org.nrg.xams.xchange.services.storage;

import org.nrg.xams.xchange.services.storage.domain.entities.ArchiveEntry;
import org.nrg.xams.xchange.services.storage.domain.entities.ArchiveFolder;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

/**
 * Provides an interface for managing the object version store. This provides access to
 * previous versions of a file resource, as well as operations for versioning current
 * objects from the primary archive.
 */
public interface XChangeVersionStore {
    ArchiveEntry archive(Path archivePath, Resource source, Path fullPath) throws IOException;

    /**
     * Archives the specified entry and replaces it with the content from the resource.
     *
     * @param entry  The archive entry to archive.
     * @param source The source with which to replace the existing archive entry.
     *
     * @return The path at which the archived entry has been stored.
     */
    Path archive(final ArchiveEntry entry, final Resource source);

    /**
     * Archives the specified entries and replaces each with the content from the corresponding
     * resource.
     *
     * @param entries The archive entries to archive.
     *
     * @return The path at which the archived entries have been stored.
     */
    List<Path> archive(final Map<ArchiveEntry, Resource> entries);

    /**
     * Archives the specified entry. This effectively functions as a delete.
     *
     * @param entry  The archive entry to archive.
     *
     * @return The path at which the archived entry has been stored.
     */
    Path archive(final ArchiveEntry entry);

    /**
     * Archives the specified entries. This effectively functions as a bulk delete.
     *
     * @param entries The archive entries to archive.
     *
     * @return The path at which the archived entries have been stored.
     */
    List<Path> archive(final List<ArchiveEntry> entries);

    Path getPath(final ArchiveFolder archiveFolder);

    Path getPath(final ArchiveEntry entry);
}
