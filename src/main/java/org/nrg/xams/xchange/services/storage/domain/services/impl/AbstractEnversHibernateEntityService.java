package org.nrg.xams.xchange.services.storage.domain.services.impl;

import lombok.extern.slf4j.Slf4j;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntityService;
import org.nrg.framework.orm.hibernate.BaseHibernateEntity;
import org.nrg.xams.xchange.services.storage.domain.auditing.EntityHistory;
import org.nrg.xams.xchange.services.storage.domain.entities.ArchiveEntry;
import org.nrg.xams.xchange.services.storage.domain.entities.ArchiveFolder;
import org.nrg.xams.xchange.services.storage.domain.repositories.AbstractEnversHibernateDAO;
import org.nrg.xams.xchange.services.storage.domain.services.EnversHibernateEntityService;

import javax.annotation.Nonnull;
import javax.transaction.Transactional;
import java.nio.file.Path;
import java.util.List;
import java.util.Set;

@Transactional
@Slf4j
public abstract class AbstractEnversHibernateEntityService<E extends BaseHibernateEntity, DAO extends AbstractEnversHibernateDAO<E>> extends AbstractHibernateEntityService<E, DAO> implements EnversHibernateEntityService<E> {
    public E findByName(final String name) {
        return getDao().findByUniqueProperty("name", name);
    }

    public E findByPath(final Path path) {
        return getDao().findByUniqueProperty("path", path.toString());
    }

    public E findByPath(final String path) {
        return getDao().findByUniqueProperty("path", path);
    }

    @Override
    public List<EntityHistory<E>> getHistory(final long id) {
        try {
            final List<EntityHistory<E>> history = getEnversDao().getHistory(id);
            // TODO: This is a hack to get around Envers steadfastly refusing to initialize collections. The archive folders to have their entry lists init'ed so that needs to be referenced.
            // TODO: This includes the logging statement! That's the reference that causes the collection to be loaded.
            history.stream().map(EntityHistory::getEntity).forEach(entity -> {
                final ArchiveFolder archiveFolder;
                if (entity instanceof ArchiveFolder) {
                    archiveFolder = ((ArchiveFolder) entity);
                } else if (entity instanceof ArchiveEntry) {
                    archiveFolder = ((ArchiveEntry) entity).getArchiveFolder();
                } else {
                    archiveFolder = null;
                }
                if (archiveFolder != null) {
                    final Set<ArchiveEntry> entries = archiveFolder.getEntries();
                    log.debug("Found {} entries for the archive folder {}", entries.size(), entity.getId());
                }
            });
            return history;
        } finally {
            getEnversDao().flush();
        }
    }

    @Override
    public List<EntityHistory<E>> getHistory(final E entity) {
        return getHistory(entity.getId());
    }

    @Override
    public EntityHistory<E> getHistory(final long id, final int revision) {
        try {
            return getEnversDao().getHistory(id, revision);
        } finally {
            getEnversDao().flush();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public List<Number> getRevisions(final long id) {
        return getDao().getRevisions(id);
    }

    @Override
    public E getRevision(final long id, @Nonnull final Number revision) {
        return getEnversDao().getRevision(id, revision);
    }

    @Override
    public int getLatestRevision(final long id) {
        return getRevisions(id).size();
    }

    @Override
    public void delete(final List<E> entities) {
        entities.forEach(this::delete);
    }

    protected DAO getEnversDao() {
        //noinspection unchecked
        return super.getDao();
    }
}
