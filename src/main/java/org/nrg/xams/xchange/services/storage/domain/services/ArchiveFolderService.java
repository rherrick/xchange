package org.nrg.xams.xchange.services.storage.domain.services;

import org.nrg.xams.xchange.services.storage.domain.entities.ArchiveFolder;

import java.nio.file.Path;

public interface ArchiveFolderService extends EnversHibernateEntityService<ArchiveFolder> {
    ArchiveFolder findByName(final String name);

    ArchiveFolder findByPath(final Path path);

    ArchiveFolder findByPath(final String path);

    boolean exists(final String name);

    boolean exists(final Path path);
}
