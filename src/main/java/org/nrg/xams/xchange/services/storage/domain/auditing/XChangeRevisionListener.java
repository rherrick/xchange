package org.nrg.xams.xchange.services.storage.domain.auditing;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.envers.EntityTrackingRevisionListener;
import org.hibernate.envers.RevisionType;
import org.nrg.xams.xchange.services.storage.domain.entities.XChangeRevisionEntity;

import java.io.Serializable;

@Slf4j
public class XChangeRevisionListener implements EntityTrackingRevisionListener {
    @Override
    public void newRevision(final Object revisionEntity) {
        log.debug("Got a revision entity {}", revisionEntity);
        ((XChangeRevisionEntity) revisionEntity).setUsername("rherrick");
    }

    @Override
    public void entityChanged(final Class entityClass, final String entityName, final Serializable entityId, final RevisionType revisionType, final Object revisionEntity) {
        log.debug("An entity of type {} with ID {} changed, name {}, type of revision {}, revision entity", entityClass, entityId, entityName, revisionType, revisionEntity);
        ((XChangeRevisionEntity) revisionEntity).setUsername("rherrick");
    }
}
