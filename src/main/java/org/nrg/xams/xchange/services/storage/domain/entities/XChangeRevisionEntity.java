package org.nrg.xams.xchange.services.storage.domain.entities;

import org.hibernate.envers.DefaultRevisionEntity;
import org.hibernate.envers.RevisionEntity;
import org.nrg.xams.xchange.services.storage.domain.auditing.XChangeRevisionListener;

import javax.persistence.Entity;

@Entity
@RevisionEntity(XChangeRevisionListener.class)
public class XChangeRevisionEntity extends DefaultRevisionEntity {
    public String getUsername() {
        return _username;
    }

    public void setUsername(final String username) {
        _username = username;
    }

    private String _username;
}
