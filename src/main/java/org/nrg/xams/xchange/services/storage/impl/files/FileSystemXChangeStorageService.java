package org.nrg.xams.xchange.services.storage.impl.files;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.nrg.framework.exceptions.NotFoundException;
import org.nrg.xams.xchange.services.storage.XChangeProperties;
import org.nrg.xams.xchange.services.storage.XChangeVersionStore;
import org.nrg.xams.xchange.services.storage.domain.auditing.EntityHistory;
import org.nrg.xams.xchange.services.storage.domain.entities.ArchiveEntry;
import org.nrg.xams.xchange.services.storage.domain.entities.ArchiveFolder;
import org.nrg.xams.xchange.services.storage.domain.services.ArchiveEntryService;
import org.nrg.xams.xchange.services.storage.domain.services.ArchiveFolderService;
import org.nrg.xams.xchange.services.storage.exceptions.FileNotFoundException;
import org.nrg.xams.xchange.services.storage.exceptions.XChangeStorageServiceException;
import org.nrg.xams.xchange.services.storage.impl.AbstractXChangeStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.PathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Profile("default")
@Lazy
@Slf4j
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(prefix = "_")
public class FileSystemXChangeStorageService extends AbstractXChangeStorageService {
    @Autowired
    public FileSystemXChangeStorageService(final XChangeProperties properties, final ArchiveFolderService archiveFolderService, final ArchiveEntryService archiveEntryService, final XChangeVersionStore versionStore) {
        super(properties, archiveFolderService, archiveEntryService, versionStore);
        _rootLocation = Paths.get(properties.getStorageLocation());
        log.info("Instantiating FileSystemXChangeStorageService with root location set to {}", _rootLocation.toAbsolutePath().toString());
    }

    @PostConstruct
    @Override
    public void init() {
        try {
            if (Files.exists(_rootLocation)) {
                log.info("The root location '{}' already exists. Continuing.", _rootLocation.toAbsolutePath().toString());
            } else {
                log.info("Creating FileSystemXChangeStorageService root location at '{}'", _rootLocation.toAbsolutePath().toString());
                Files.createDirectory(_rootLocation);
            }
        } catch (IOException e) {
            throw new XChangeStorageServiceException("Could not initialize storage", e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Transactional
    @Override
    public ArchiveFolder store(final String name, final Path rootPath) {
        return this.getArchiveFolderService().exists(rootPath) ? this.getArchiveFolderService().findByPath(rootPath) : this.getArchiveFolderService().create(name, rootPath);
    }

    /**
     * {@inheritDoc}
     */
    @Transactional
    @Override
    public ArchiveFolder store(final String name, final Path rootPath, final Map<Path, Resource> entries) {
        return store(store(name, rootPath), entries);
    }

    /**
     * {@inheritDoc}
     */
    @Transactional
    @Override
    public ArchiveFolder store(final Path rootPath, final Map<Path, Resource> entries) {
        if (!this.getArchiveFolderService().exists(rootPath)) {
            throw new RuntimeException(new NotFoundException(rootPath.toString()));
        }
        return store(this.getArchiveFolderService().findByPath(rootPath), entries);
    }

    /**
     * {@inheritDoc}
     */
    @Transactional
    @Override
    public ArchiveFolder store(final ArchiveFolder archiveFolder, final Map<Path, Resource> entries) {
        final Path                 archiveFolderRoot = getRootLocation().resolve(archiveFolder.getPath());
        final Map<Path, Throwable> errors            = new HashMap<>();

        entries.forEach((path, source) -> {
            final Path destination = archiveFolderRoot.resolve(path);
            final Path parent      = destination.getParent();

            try {
                if (!parent.toFile().exists()) {
                    Files.createDirectories(parent);
                }

                final Set<ArchiveEntry>      archiveFolderEntries = archiveFolder.getEntries();
                final Optional<ArchiveEntry> optional             = archiveFolderEntries == null ? Optional.empty() : archiveFolderEntries.stream().filter(item -> item.getPathAsPath().equals(path)).findFirst();
                final ArchiveEntry           entry;
                if (optional.isPresent()) {
                    entry = optional.get();
                    final Path archivePath = getVersionStore().archive(entry, source);
                    log.debug("Entry '{}' archived to path {}", entry, archivePath);
                } else if (destination.toFile().exists()) {
                    entry = getArchiveEntryService().findByPath(path);
                    final Path archivePath = getVersionStore().archive(entry, source);
                    log.debug("Entry '{}' archived to path {}", entry, archivePath);
                } else {
                    entry = getVersionStore().archive(path, source, destination);
                    log.debug("Entry '{}' with checksum '{}' created at path {}", entry, entry.getDigest(), path);
                    archiveFolder.addEntry(entry);
                }
            } catch (IOException e) {
                log.error("An error occurred trying to process the path '{}'", path, e);
                errors.put(path, e);
            }
        });
        if (!errors.isEmpty()) {
            log.error("{} errors occurred trying to process the storage operation", errors.size());
            throw new RuntimeException(errors.size() + " errors occurred trying to process the storage operation");
        }
        getArchiveFolderService().update(archiveFolder);
        getArchiveFolderService().flush();
        getArchiveEntryService().flush();
        getArchiveFolderService().refresh(archiveFolder);
        return archiveFolder;
    }

    @Transactional
    @Override
    public ArchiveFolder delete(final ArchiveFolder archiveFolder, final List<Path> paths) {
        final Set<ArchiveEntry>  entries      = archiveFolder.getEntries();
        final List<ArchiveEntry> removals     = entries.stream().filter(entry -> paths.contains(entry.getPathAsPath())).collect(Collectors.toList());
        final List<Path>         archivePaths = getVersionStore().archive(removals);
        entries.removeAll(removals);
        this.getArchiveFolderService().update(archiveFolder);
        log.info("Deleted {} entries from archive folder '{}', stored to: {}", paths.size(), archiveFolder.getName(), archivePaths);
        return archiveFolder;
    }

    @Transactional
    @Override
    public ArchiveFolder getArchiveFolder(final Path path) throws NotFoundException {
        final ArchiveFolder archiveFolder = this.getArchiveFolderService().findByPath(path);
        if (archiveFolder == null) {
            throw new RuntimeException("Can't find an archive folder with path " + path.toString());
        }
        return this.getArchiveFolderService().get(archiveFolder.getId());
    }

    @Transactional
    @Override
    public ArchiveFolder getArchiveFolder(final Path path, final Number version) {
        final ArchiveFolder archiveFolder = this.getArchiveFolderService().findByPath(path);
        if (archiveFolder == null) {
            throw new RuntimeException("Can't find an archive folder with path " + path.toString());
        }
        return this.getArchiveFolderService().getRevision(archiveFolder.getId(), version);
    }

    @Transactional
    @Override
    public List<Number> getRevisions(final ArchiveFolder archiveFolder) {
        return this.getArchiveFolderService().getRevisions(archiveFolder.getId());
    }

    @Transactional
    @Override
    public List<EntityHistory<ArchiveFolder>> getHistory(final ArchiveFolder archiveFolder) {
        return this.getArchiveFolderService().getHistory(archiveFolder.getId());
    }

    @Transactional
    @Override
    public ArchiveEntry getEntry(final Path archiveEntryPath, final Path entryPath) throws NotFoundException {
        final ArchiveFolder archiveFolder = this.getArchiveFolderService().findByPath(archiveEntryPath);
        if (archiveFolder == null) {
            throw new RuntimeException("Can't find an archive folder with path " + archiveEntryPath.toString());
        }
        final ArchiveEntry entry = this.getArchiveEntryService().findByPath(entryPath);
        if (entry == null) {
            throw new RuntimeException("Can't find a entry with path " + entryPath.toString());
        }
        if (!entry.getArchiveFolder().equals(archiveFolder)) {
            throw new RuntimeException("The entry " + entryPath.toString() + " is not contained in the archive folder " + archiveEntryPath.toString());
        }
        return this.getArchiveEntryService().get(entry.getId());
    }

    @Transactional
    @Override
    public List<Number> getRevisions(final ArchiveEntry entry) {
        return this.getArchiveEntryService().getRevisions(entry.getId());
    }

    @Transactional
    @Override
    public List<EntityHistory<ArchiveEntry>> getHistory(final ArchiveEntry entry) {
        return this.getArchiveEntryService().getHistory(entry.getId());
    }

    @Transactional
    @Override
    public ArchiveEntry getEntry(final Path archiveEntryPath, final Path entryPath, final Number version) {
        final ArchiveFolder archiveFolder = this.getArchiveFolderService().findByPath(archiveEntryPath);
        if (archiveFolder == null) {
            throw new RuntimeException("Can't find an archive folder with path " + archiveEntryPath.toString());
        }
        final ArchiveEntry entry = this.getArchiveEntryService().findByPath(entryPath);
        if (entry == null) {
            throw new RuntimeException("Can't find a entry with path " + entryPath.toString());
        }
        if (!entry.getArchiveFolder().equals(archiveFolder)) {
            throw new RuntimeException("The entry " + entryPath.toString() + " is not contained in the archive folder " + archiveEntryPath.toString());
        }
        return this.getArchiveEntryService().getRevision(entry.getId(), version);
    }

    @Transactional
    @Override
    public Resource getResource(final ArchiveEntry entry) {
        return new PathResource(getVersionStore().getPath(entry));
    }

    @Override
    public Resource getResource(final ArchiveEntry entry, final Number revision) {
        return null;
    }

    @Override
    public Map<Path, Resource> getResources(final ArchiveFolder archiveFolder) {
        return archiveFolder.getEntries().stream().collect(Collectors.toMap(ArchiveEntry::getPathAsPath, this::getResource));
    }

    @Override
    public Map<Path, Resource> getResources(final ArchiveFolder archiveFolder, final Number revision) {
        final ArchiveFolder versioned = this.getArchiveFolderService().getRevision(archiveFolder.getId(), revision);
        return versioned.getEntries().stream().collect(Collectors.toMap(ArchiveEntry::getPathAsPath, this::getResource));
    }

    @Override
    public void store(final MultipartFile file) {
    }

    @Override
    public void store(final MultipartFile file, final Path path) {
        try {
            if (file.isEmpty()) {
                throw new XChangeStorageServiceException("Failed to store empty file " + file.getOriginalFilename());
            }
            Files.copy(file.getInputStream(), getRootLocation().resolve(path).resolve(file.getOriginalFilename()));
        } catch (IOException e) {
            throw new XChangeStorageServiceException("Failed to store file " + file.getOriginalFilename(), e);
        }
    }

    @Override
    public Stream<Path> loadAll() {
        try {
            return Files.walk(getRootLocation(), 1)
                        .filter(path -> !path.equals(getRootLocation()))
                        .map(getRootLocation()::relativize);
        } catch (IOException e) {
            throw new XChangeStorageServiceException("Failed to read stored files", e);
        }

    }

    @Override
    public Path load(final String filename) {
        return getRootLocation().resolve(filename);
    }

    @Override
    public Resource loadAsResource(final String filename) {
        try {
            final Path     file     = load(filename);
            final Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new FileNotFoundException("Could not read file: " + filename);

            }
        } catch (MalformedURLException e) {
            throw new FileNotFoundException("Could not read file: " + filename, e);
        }
    }

    @Override
    public void deleteAll() {
        FileSystemUtils.deleteRecursively(_rootLocation.toFile());
    }

    private final Path _rootLocation;
}
