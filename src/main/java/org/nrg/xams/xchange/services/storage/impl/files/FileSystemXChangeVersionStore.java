package org.nrg.xams.xchange.services.storage.impl.files;

import com.twmacinta.util.MD5;
import lombok.Getter;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.nrg.xams.xchange.services.storage.XChangeProperties;
import org.nrg.xams.xchange.services.storage.XChangeVersionStore;
import org.nrg.xams.xchange.services.storage.domain.entities.ArchiveEntry;
import org.nrg.xams.xchange.services.storage.domain.entities.ArchiveFolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import static lombok.AccessLevel.PRIVATE;

@Service
@Slf4j
@Getter(PRIVATE)
@Accessors(prefix = "_")
public class FileSystemXChangeVersionStore implements XChangeVersionStore {
    @Autowired
    public FileSystemXChangeVersionStore(final XChangeProperties properties) {
        _rootLocation = Paths.get(properties.getStorageLocation());
        log.info("Instantiating FileSystemXChangeVersionStore with root location set to {}", _rootLocation.toAbsolutePath().toString());
    }

    @Override
    public ArchiveEntry archive(final Path archivePath, final Resource source, final Path destination) throws IOException {
        if (isInplaceCommit(source, destination)) {
            log.debug("The source and destination files were the same (in-place commit), so no files were copied: {}", destination);
        } else {
            Files.copy(source.getInputStream(), destination);
            log.debug("Copied entry file from source '{}' to '{}'", source, destination);
        }
        return new ArchiveEntry(archivePath, archivePath.getFileName().toString(), getDigest(destination));
    }

    @Override
    public Path archive(final ArchiveEntry entry, final Resource source) {
        final Path archivePath = archive(entry);
        final Path destination = getPath(entry);
        try {
            Files.copy(source.getInputStream(), destination, REPLACE_EXISTING);
            final String digest = getDigest(destination);
            log.debug("Entry '{}' with checksum '{}' archived to path {}, replaced with entry update with digest {}", entry, entry.getDigest(), archivePath, digest);
            entry.setDigest(digest);
            return archivePath;
        } catch (IOException e) {
            throw new RuntimeException("An error occurred trying to archive the entry " + entry + " to the destination " + destination, e);
        }
    }

    @Override
    public List<Path> archive(final Map<ArchiveEntry, Resource> entries) {
        return entries.entrySet().stream().map(entry -> archive(entry.getKey(), entry.getValue())).collect(Collectors.toList());
    }

    @Override
    public Path archive(final ArchiveEntry entry) {
        try {
            final Path entryPath     = getPath(entry);
            final Path archiveFolder = getArchivePath(entry);
            if (!archiveFolder.toFile().exists()) {
                Files.createDirectories(archiveFolder);
            }
            final Path archiveFile = archiveFolder.resolve(entry.getDigest());
            if (!archiveFile.toFile().exists()) {
                Files.move(entryPath, archiveFile);
                log.debug("Moved entry resource '{}' to archive path '{}'", entryPath, archiveFile);
            } else {
                log.debug("Tried to move entry resource '{}' to archive path '{}', but that file is already there. Not overwriting, this means the file is \"updated\" but didn't change.", entryPath, archiveFile);
            }
            return archiveFile;
        } catch (IOException e) {
            log.error("", e);
            return null;
        }
    }

    @Override
    public List<Path> archive(final List<ArchiveEntry> entries) {
        return entries.stream().map(this::archive).collect(Collectors.toList());
    }

    @Override
    public Path getPath(final ArchiveFolder archiveFolder) {
        return getRootLocation().resolve(archiveFolder.getPathAsPath());
    }

    @Override
    public Path getPath(final ArchiveEntry entry) {
        return getPath(entry.getArchiveFolder()).resolve(entry.getPathAsPath());
    }

    private Path getArchivePath(final ArchiveEntry entry) {
        return getPath(entry).getParent().resolve(".versions").resolve(entry.getName());
    }

    private static String getDigest(final Path source) throws IOException {
        return MD5.asHex(MD5.getHash(source.toFile()));
    }

    private static boolean isInplaceCommit(final Resource source, final Path destination) {
        try {
            return Files.isSameFile(Paths.get(source.getURI()), destination);
        } catch (IOException ignored) {
            // This means the Resource.getURI() call doesn't work on this particular resource.
            // That's OK, let's try getFile() just for kicks.
        }
        try {
            return Files.isSameFile(source.getFile().toPath(), destination);
        } catch (IOException ignored) {
            // Second verse same as the first, that's all we'll try. This usually means the
            // resource was created with an input stream and so can't be referenced by path.
            // In that case, we know that it's inherently not an in-place commit.
        }
        return false;
    }

    private final Path _rootLocation;
}
