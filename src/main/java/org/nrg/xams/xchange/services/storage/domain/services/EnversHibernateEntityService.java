package org.nrg.xams.xchange.services.storage.domain.services;

import org.nrg.framework.orm.hibernate.BaseHibernateEntity;
import org.nrg.framework.orm.hibernate.BaseHibernateService;
import org.nrg.xams.xchange.services.storage.domain.auditing.EntityHistory;

import java.util.List;

public interface EnversHibernateEntityService<E extends BaseHibernateEntity> extends BaseHibernateService<E> {
    List<EntityHistory<E>> getHistory(final long id);

    List<EntityHistory<E>> getHistory(final E entity);

    EntityHistory<E> getHistory(final long id, final int revision);

    int getLatestRevision(final long id);

    void delete(final List<E> entities);
}
