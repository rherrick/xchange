package org.nrg.xams.xchange.services.storage.domain.auditing;

import org.hibernate.Hibernate;
import org.hibernate.envers.RevisionType;
import org.nrg.framework.orm.hibernate.BaseHibernateEntity;
import org.nrg.xams.xchange.services.storage.domain.entities.XChangeRevisionEntity;

public class AuditQueryResultUtils {
    private AuditQueryResultUtils() {
    }

    public static <T extends BaseHibernateEntity> AuditQueryResult<T> getAuditQueryResult(final Object[] item, final Class<T> type) {
        // Early exit, if no item or not enough data given:
        return (item != null && item.length == 3) ? new AuditQueryResult<>(getEntity(type, item[0]),
                                                                           item[1] instanceof XChangeRevisionEntity ? (XChangeRevisionEntity) item[1] : null,
                                                                           item[2] instanceof RevisionType ? (RevisionType) item[2] : null)
                                                  : null;
    }

    private static <T> T getEntity(final Class<T> type, final Object object) {
        if (!type.isInstance(object)) {
            return null;
        }
        final T entity = type.cast(object);
        // TODO: All of this is a hack to get around the fact that Envers steadfastly refuses to eagerly initialize collections.
        Hibernate.initialize(entity);
        return entity;
    }
}
