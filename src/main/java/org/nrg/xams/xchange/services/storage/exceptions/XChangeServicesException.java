package org.nrg.xams.xchange.services.storage.exceptions;

public class XChangeServicesException extends RuntimeException {
    public XChangeServicesException(final String message) {
        super(message);
    }

    public XChangeServicesException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
