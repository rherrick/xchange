package org.nrg.xams.xchange.services.storage;

import com.twmacinta.util.MD5;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.StreamSupport;

@SuppressWarnings("WeakerAccess")
@Slf4j
public class PathAndFileUtils {
    public static Path getCommonAncestor(final Collection<Path> paths) {
        return paths.stream().reduce(PathAndFileUtils::getCommonAncestor).orElse(Paths.get(""));
    }

    public static Path getCommonAncestor(final Path... paths) {
        return Arrays.stream(paths).reduce(PathAndFileUtils::getCommonAncestor).orElse(Paths.get(""));
    }

    public static Path getCommonAncestor(final Path path1, final Path path2) {
        final List<Path> split1 = atomize(path1);
        if (split1.isEmpty()) {
            return path2;
        }

        final List<Path> split2 = atomize(path2);
        final List<Path> common = new ArrayList<>();
        IntStream.range(0, split1.size()).forEach(index -> {
            if (common.size() == index) {
                final Path subPath1 = split1.get(index);
                final Path subPath2 = split2.get(index);
                if (subPath1.equals(subPath2)) {
                    common.add(subPath1);
                }
            }
        });
        return common.stream().reduce(Path::resolve).orElse(Paths.get(""));
    }

    public static String getFileDigest(final File file) {
        try {
            return MD5.asHex(MD5.getHash(file));
        } catch (IOException e) {
            log.error("Encountered an error trying to create a checksum for the file '{}'", file, e);
            return "";
        }
    }

    public static List<Path> atomize(final Path path) {
        return StreamSupport.stream(path.spliterator(), false).collect(Collectors.toList());
    }

    public static List<Path> convertPathToHierarchy(final Path path) {
        return convertPathsToHierarchy(atomize(path));
    }

    public static List<Path> convertPathsToHierarchy(final List<Path> atomized) {
        return IntStream.range(0, atomized.size() + 1).mapToObj(index -> atomized.stream().limit(index).reduce(Path::resolve).orElse(null)).filter(Objects::nonNull).collect(Collectors.toList());
    }

    public static Map<Path, Boolean> checkHierarchyForExistence(final List<Path> hierarchy) {
        return checkHierarchyForExistence(null, hierarchy);
    }

    public static Map<Path, Boolean> checkHierarchyForExistence(final Path root, final List<Path> hierarchy) {
        return hierarchy.stream().collect(Collectors.toMap(Function.identity(), path -> root != null ? root.resolve(path).toFile().exists() : path.toFile().exists()));
    }
}
