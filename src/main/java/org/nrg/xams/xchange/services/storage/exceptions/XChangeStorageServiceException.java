package org.nrg.xams.xchange.services.storage.exceptions;

public class XChangeStorageServiceException extends XChangeServicesException {
    public XChangeStorageServiceException(final String message) {
        super(message);
    }

    public XChangeStorageServiceException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
