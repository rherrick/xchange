package org.nrg.xams.xchange.services.storage.domain.auditing;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import org.hibernate.envers.RevisionType;
import org.nrg.xams.xchange.services.storage.domain.entities.XChangeRevisionEntity;

@Getter
@Accessors(prefix = "_")
@RequiredArgsConstructor
public class AuditQueryResult<T> {
    private final T                     _entity;
    private final XChangeRevisionEntity _revision;
    private final RevisionType          _type;
}
