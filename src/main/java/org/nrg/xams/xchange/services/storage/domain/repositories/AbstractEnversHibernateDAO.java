package org.nrg.xams.xchange.services.storage.domain.repositories;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.SessionFactory;
import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.query.AuditEntity;
import org.hibernate.envers.query.AuditQuery;
import org.nrg.framework.orm.hibernate.AbstractHibernateDAO;
import org.nrg.framework.orm.hibernate.BaseHibernateEntity;
import org.nrg.xams.xchange.services.storage.domain.auditing.AuditQueryResult;
import org.nrg.xams.xchange.services.storage.domain.auditing.AuditQueryUtils;
import org.nrg.xams.xchange.services.storage.domain.auditing.EntityHistory;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class AbstractEnversHibernateDAO<E extends BaseHibernateEntity> extends AbstractHibernateDAO<E> {
    @SuppressWarnings("WeakerAccess")
    protected AbstractEnversHibernateDAO() {
        super();
    }

    @SuppressWarnings("unused")
    protected AbstractEnversHibernateDAO(final Class<E> clazz) {
        super(clazz);
    }

    @SuppressWarnings("unused")
    protected AbstractEnversHibernateDAO(final SessionFactory factory) {
        super(factory);
    }

    @SuppressWarnings("unchecked")
    public List<EntityHistory<E>> getHistory(final long id) {
        final AuditQuery                query   = getAuditReader().createQuery().forRevisionsOfEntity(getParameterizedType(), false, true).add(AuditEntity.id().eq(id));
        final List<AuditQueryResult<E>> results = AuditQueryUtils.getAuditQueryResults(query, getParameterizedType());
        return results.stream().map(EntityHistory::new).collect(Collectors.toList());
    }

    @SuppressWarnings({"unchecked", "unused"})
    public EntityHistory<E> getHistory(final long id, final int revision) {
        final AuditQuery                query   = getAuditReader().createQuery().forRevisionsOfEntity(getParameterizedType(), false, true).add(AuditEntity.id().eq(id)).add(AuditEntity.revisionNumber().eq(revision));
        final List<AuditQueryResult<E>> results = AuditQueryUtils.getAuditQueryResults(query, getParameterizedType());
        if (results.isEmpty()) {
            log.info("Found no results for entity of type {} with ID {} and revision {}", getParameterizedType(), id, revision);
            return null;
        }
        log.info("Found {} results for entity of type {} with ID {} and revision {}", results.size(), getParameterizedType(), id, revision);
        return new EntityHistory<>(results.get(0));
    }

    @Override
    public List<Number> getRevisions(final long id) {
        return getAuditReader().getRevisions(getParameterizedType(), id);
    }

    @Override
    public E getRevision(final long id, final Number revision) {
        if (revision.intValue() < 1) {
            throw new IllegalArgumentException("Revision numbers for entities start at 1, anything less than that is invalid.");
        }
        final List<Number> revisions = getRevisions(id);
        if (revisions.size() < revision.intValue()) {
            throw new IllegalArgumentException("There are only " + revisions.size() + " revisions for entity with ID " + id + ". The requested revision number " + revision + " is invalid.");
        }
        final Number version = revisions.get(revision.intValue() - 1);
        return getAuditReader().find(getParameterizedType(), id, version);
    }

    @SuppressWarnings("WeakerAccess")
    protected AuditReader getAuditReader() {
        return AuditReaderFactory.get(getSession());
    }
}