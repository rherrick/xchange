package org.nrg.xams.xchange.services.storage;

import org.nrg.framework.exceptions.NotFoundException;
import org.nrg.xams.xchange.services.storage.domain.auditing.EntityHistory;
import org.nrg.xams.xchange.services.storage.domain.entities.ArchiveEntry;
import org.nrg.xams.xchange.services.storage.domain.entities.ArchiveFolder;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

/**
 * Defines the interface for a storage service.
 */
public interface XChangeStorageService {
    /**
     * Initializes the storage service.
     */
    void init();

    /**
     * Gets an archive folder with the specified name and path. If an archive folder with that name and
     * path already exists, that archive folder is returned. If no archive folder exists with the specified name and
     * path, a new archive folder is created. This is the same as versions of this method with a map of entries
     * but it only creates or retrieves the archive folder.
     *
     * @param name     The name of the resource archive folder.
     * @param rootPath The root path where the files should be stored.
     *
     * @return The {@link ArchiveFolder} object for the specified name and path.
     */
    ArchiveFolder store(final String name, final Path rootPath);

    /**
     * Stores the submitted files under the specified root path. The key of the entries map indicates
     * the relative path under the root path to which the resource entry should be stored. The value of
     * the entries map contains the resource from which the data to be stored can be read. A null value
     * indicates an existing file to be deleted. Note that, if an archive folder with the specified name
     * and path already exists, the entries are added to that archive folder. If no archive folder exists
     * with the specified name and path, a new archive folder is created and the entries are added to the
     * new archive folder.
     *
     * @param name     The name of the resource archive folder.
     * @param rootPath The root path where the files should be stored.
     * @param entries  The entries to store.
     *
     * @return The {@link ArchiveFolder} object for the specified name and path.
     */
    ArchiveFolder store(final String name, final Path rootPath, final Map<Path, Resource> entries);

    /**
     * Stores the submitted files under the specified root path. The key of the entries map indicates
     * the relative path under the root path to which the resource entry should be stored. The value of
     * the entries map contains the resource from which the data to be stored can be read. A null value
     * indicates an existing file to be deleted.
     *
     * Unlike {@link #store(String, Path, Map) the other version of this method}, an archive folder with
     * the specified path must already exist.
     *
     * @param rootPath The root path where the files should be stored.
     * @param entries  The entries to store.
     *
     * @return The {@link ArchiveFolder} object for the specified path.
     */
    ArchiveFolder store(final Path rootPath, final Map<Path, Resource> entries);

    /**
     * Stores the submitted entries under the specified archive folder. The key of the entries map indicates
     * the relative path under the root path to which the resource entry should be stored. The value of
     * the entries map contains the resource from which the data to be stored can be read. A null value
     * indicates an existing file to be deleted.
     *
     * @param archiveFolder The archive folder where the entries should be stored.
     * @param entries       The entries to store.
     *
     * @return The {@link ArchiveFolder} object for the specified path.
     */
    ArchiveFolder store(final ArchiveFolder archiveFolder, final Map<Path, Resource> entries);

    /**
     * Deletes the entries specified by path from the {@link ArchiveFolder archive folder}. The file resources are
     * relocated to the version store without being replaced. The corresponding {@link ArchiveEntry archive folder
     * entries} are marked as deleted.
     *
     * @param archiveFolder The archive folder from which the entries should be deleted.
     * @param entries       The entries to be deleted.
     *
     * @return The updated archive folder object.
     */
    ArchiveFolder delete(final ArchiveFolder archiveFolder, final List<Path> entries);

    /**
     * Gets the archive folder with the specified {@link ArchiveFolder#getPath() path}.
     *
     * @param path The path for the archive folder to retrieve.
     *
     * @return The {@link ArchiveFolder} instance with the specified path.
     *
     * @throws NotFoundException When no {@link ArchiveFolder} instance is associated with the specified path.
     */
    ArchiveFolder getArchiveFolder(final Path path) throws NotFoundException;

    /**
     * Gets the specified revision of the archive folder with the {@link ArchiveFolder#getPath() specified path}.
     *
     * @param path     The path for the archive folder to retrieve.
     * @param revision The revision of the archive folder to retrieve.
     *
     * @return The specified revision of the {@link ArchiveFolder} instance with the {@link ArchiveFolder#getPath() specified
     *         path}.
     *
     * @throws NotFoundException When no {@link ArchiveFolder} instance is associated with the specified path or the
     *                           specified revision doesn't exist.
     */
    ArchiveFolder getArchiveFolder(final Path path, final Number revision) throws NotFoundException;

    /**
     * Returns a list of the revision numbers for the specified archive folder.
     *
     * @param archiveFolder The archive folder to retrieve.
     *
     * @return A list of the revision numbers for the archive folder.
     */
    List<Number> getRevisions(ArchiveFolder archiveFolder);

    /**
     * Returns a list of the different versions for the specified archive folder.
     *
     * @param archiveFolder The archive folder to retrieve.
     *
     * @return A list of the different versions for the archive folder.
     */
    List<EntityHistory<ArchiveFolder>> getHistory(ArchiveFolder archiveFolder);

    /**
     * Gets the {@link ArchiveEntry entry} with the specified path from the {@link ArchiveFolder archive folder} with the specified
     * {@link ArchiveFolder#getPath() path}.
     *
     * @param archiveEntryPath The path for the archive folder to retrieve.
     * @param entryPath        The path for the entry to retrieve.
     *
     * @return The {@link ArchiveEntry} instance with the specified path and archive folder.
     *
     * @throws NotFoundException When no {@link ArchiveFolder} or {@link ArchiveEntry} instances are associated with the
     *                           specified paths.
     */
    ArchiveEntry getEntry(final Path archiveEntryPath, final Path entryPath) throws NotFoundException;

    /**
     * Gets the {@link ArchiveEntry entry} with the specified path from the {@link ArchiveFolder archive folder} with the specified
     * {@link ArchiveFolder#getPath() path} and revision.
     *
     * @param archiveEntryPath The path for the archive folder to retrieve.
     * @param entryPath        The path for the entry to retrieve.
     * @param revision         The revision of the archive folder to retrieve.
     *
     * @return The {@link ArchiveEntry} instance with the specified path, archive folder, and revision.
     *
     * @throws NotFoundException When no {@link ArchiveFolder} or {@link ArchiveEntry} instances are associated with the
     *                           specified paths.
     */
    ArchiveEntry getEntry(final Path archiveEntryPath, final Path entryPath, final Number revision) throws NotFoundException;

    /**
     * Returns a list of the revision numbers for the specified entry.
     *
     * @param entry The entry to retrieve.
     *
     * @return A list of the revision numbers for the entry.
     */
    List<Number> getRevisions(ArchiveEntry entry);

    /**
     * Returns a list of the different versions for the specified entry.
     *
     * @param entry The entry to retrieve.
     *
     * @return A list of the different versions for the entry.
     */
    List<EntityHistory<ArchiveEntry>> getHistory(ArchiveEntry entry);

    Resource getResource(final ArchiveEntry entry);

    Resource getResource(final ArchiveEntry entry, final Number revision);

    Map<Path, Resource> getResources(final ArchiveFolder archiveFolder);

    Map<Path, Resource> getResources(final ArchiveFolder archiveFolder, final Number revision);

    /**
     * Stores the submitted file in a default location.
     *
     * @param file The file to be stored.
     */
    void store(final MultipartFile file);

    /**
     * Stores the submitted file to the submitted path. This should be a relative path so
     * that the file is stored relative to the root storage location for the implemented
     * storage service.
     *
     * @param file The file to be stored.
     * @param path The path at which the file should be stored.
     */
    void store(final MultipartFile file, final Path path);

    /**
     * Loads all stored files.
     *
     * @return A stream of all of the stored files.
     */
    Stream<Path> loadAll();

    /**
     * Gets the path to the specified file.
     *
     * @param filename The name of the specified file.
     *
     * @return The path to the specified file.
     */
    Path load(final String filename);

    /**
     * Gets the specified file as a resource object.
     *
     * @param filename The name of the specified file.
     *
     * @return The specified file as a resource object.
     */
    Resource loadAsResource(final String filename);

    /**
     * Delete all files stored in the service.
     */
    void deleteAll();
}
