package org.nrg.xams.xchange.services.storage.domain.auditing;

import org.hibernate.envers.query.AuditQuery;
import org.nrg.framework.orm.hibernate.BaseHibernateEntity;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class AuditQueryUtils {
    private AuditQueryUtils() {
    }

    public static <E extends BaseHibernateEntity> List<AuditQueryResult<E>> getAuditQueryResults(final AuditQuery query, final Class<E> targetType) {
        final List<?> results = query.getResultList();

        if (results == null || results.isEmpty()) {
            return Collections.emptyList();
        }

        // The AuditReader returns a List of Object[], where the indices are:
        //
        // 0 - The queried entity
        // 1 - The revision entity
        // 2 - The Revision Type
        //
        // We cast it into something useful for a safe access:
        return results.stream()
                      // Only use Object[] results:
                      .filter(result -> result instanceof Object[])
                      // Then convert to Object[]:
                      .map(result -> (Object[]) result)
                      // Transform into the AuditQueryResult:
                      .map(result -> AuditQueryResultUtils.getAuditQueryResult(result, targetType))
                      // And collect the Results into a List:
                      .collect(Collectors.toList());
    }
}
