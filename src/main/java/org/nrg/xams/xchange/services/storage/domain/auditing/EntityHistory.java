package org.nrg.xams.xchange.services.storage.domain.auditing;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import org.hibernate.envers.RevisionType;
import org.nrg.framework.orm.hibernate.BaseHibernateEntity;
import org.nrg.xams.xchange.services.storage.domain.entities.XChangeRevisionEntity;

@Getter
@Accessors(prefix = "_")
@RequiredArgsConstructor
public class EntityHistory<E extends BaseHibernateEntity> {
    public EntityHistory(final AuditQueryResult<E> result) {
        _entity = result.getEntity();
        _revision = result.getRevision();
        _type = result.getType();
    }

    private final E                     _entity;
    private final XChangeRevisionEntity _revision;
    private final RevisionType          _type;
}
