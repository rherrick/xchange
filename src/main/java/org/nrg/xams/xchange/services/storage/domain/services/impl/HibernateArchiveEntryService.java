package org.nrg.xams.xchange.services.storage.domain.services.impl;

import org.nrg.xams.xchange.services.storage.domain.auditing.EntityHistory;
import org.nrg.xams.xchange.services.storage.domain.entities.ArchiveEntry;
import org.nrg.xams.xchange.services.storage.domain.entities.ArchiveFolder;
import org.nrg.xams.xchange.services.storage.domain.repositories.ArchiveEntryRepository;
import org.nrg.xams.xchange.services.storage.domain.services.ArchiveEntryService;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class HibernateArchiveEntryService extends AbstractEnversHibernateEntityService<ArchiveEntry, ArchiveEntryRepository> implements ArchiveEntryService {
    @Override
    public InputStream getEntry(final ArchiveEntry entry) throws IOException {
        return Files.newInputStream(entry.getPathAsPath(), StandardOpenOption.READ);
    }

    @Override
    public InputStream getEntry(final ArchiveEntry entry, final int version) throws IOException {
        return Files.newInputStream(entry.getPathAsPath(), StandardOpenOption.READ);
    }

    @Override
    public Map<Path, List<EntityHistory<ArchiveEntry>>> getHistory(final ArchiveFolder archiveFolder) {
        return archiveFolder.getEntries().stream().collect(Collectors.toMap(ArchiveEntry::getPathAsPath, this::getHistory));
    }

    @Override
    public Map<Path, List<Number>> getRevisions(final ArchiveFolder archiveFolder) {
        return archiveFolder.getEntries().stream().collect(Collectors.toMap(ArchiveEntry::getPathAsPath, entry -> getRevisions(entry.getId())));
    }
}
