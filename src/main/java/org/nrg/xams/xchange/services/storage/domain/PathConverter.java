package org.nrg.xams.xchange.services.storage.domain;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.nio.file.Path;
import java.nio.file.Paths;

@Converter(autoApply = true)
public class PathConverter implements AttributeConverter<Path, String> {
    @Override
    public String convertToDatabaseColumn(final Path path) {
        return path.toString();
    }

    @Override
    public Path convertToEntityAttribute(final String string) {
        return Paths.get(string);
    }
}
