package org.nrg.xams.xchange.config;

import org.nrg.framework.utilities.Reflection;
import org.nrg.xams.xchange.services.storage.XChangeProperties;
import org.nrg.xams.xchange.services.storage.XChangeStorageService;
import org.nrg.xams.xchange.services.storage.XChangeVersionStore;
import org.nrg.xams.xchange.services.storage.domain.services.ArchiveFolderService;
import org.nrg.xams.xchange.services.storage.domain.services.ArchiveEntryService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource(value = "classpath:storage-${envTarget:default}.properties", ignoreResourceNotFound = true)
@ComponentScan("org.nrg.xams.xchange.services.storage.domain")
public class XChangeServicesConfig {
    @Bean
    public XChangeProperties storageProperties() {
        return new XChangeProperties();
    }

    @Bean
    public XChangeVersionStore versionStore(final XChangeProperties properties) {
        final XChangeVersionStore versionStore = Reflection.constructObjectFromParameters(_versionStoreImpl, XChangeVersionStore.class, properties);
        if (versionStore == null) {
            throw new RuntimeException("No version store object created for class '" + _versionStoreImpl + "' with parameter types XChangeProperties");
        }
        return versionStore;
    }

    @Bean
    public XChangeStorageService storageService(final XChangeProperties properties, final ArchiveFolderService archiveFolderService, final ArchiveEntryService archiveEntryService, final XChangeVersionStore versionStore) {
        final XChangeStorageService xchangeStorageService = Reflection.constructObjectFromParameters(_storageServiceImpl, XChangeStorageService.class, properties, archiveFolderService, archiveEntryService, versionStore);
        if (xchangeStorageService == null) {
            throw new RuntimeException("No storage service object created for class '" + _storageServiceImpl + "' with parameter types XChangeProperties, ArchiveFolderService, ArchiveEntryService, XChangeVersionStore");
        }
        return xchangeStorageService;
    }

    @Value("${storage.service.impl:org.nrg.xams.xchange.services.storage.impl.files.FileSystemXChangeStorageService}")
    private String _storageServiceImpl;

    @Value("${version.store.impl:org.nrg.xams.xchange.services.storage.impl.files.FileSystemXChangeVersionStore}")
    private String _versionStoreImpl;
}
